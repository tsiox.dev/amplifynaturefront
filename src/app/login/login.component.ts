import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { Client, Orga } from "../shared/models/client.model";

import * as firebase from 'firebase/app';
import * as CryptoJS from 'crypto-js';


var firebaseConfig = {
    // apiKey: "AIzaSyAEWckegKre1mypuIQHUk-8wXYt64Nu9IM",
    // authDomain: "amplifynature2.firebaseapp.com",
    // databaseURL: "https://amplifynature2.firebaseio.com",
    // projectId: "amplifynature2",
    // storageBucket: "amplifynature2.appspot.com",
    // messagingSenderId: "146345390380",
    // appId: "1:146345390380:web:998cafb93ae2e42d"

    apiKey: "AIzaSyD9iGZ8J8KoOB6eBcXSTmnZjhFGemeHcTk",
    authDomain: "amplifynature-8be2d.firebaseapp.com",
    databaseURL: "https://amplifynature-8be2d.firebaseio.com",
    projectId: "amplifynature-8be2d",
    storageBucket: "amplifynature-8be2d.appspot.com",
    messagingSenderId: "459285736048"


};
var db_app2 = firebase.default.initializeApp(firebaseConfig, 'trombinoscope');
 db_app2.auth().signInWithEmailAndPassword('frankderrien@gmail.com','WWWWWW');   

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    email: string;
    password: string;
    error= "";
    loading:Boolean = false;
    loginPage:Boolean = true;
    chooseOrga:Boolean = false;
    clientId:any = false;
    client:any = false;
    loginStyle:any = {};
    background:any = "";

    orga:Orga[] = [];
    curOrga:string;
    selectedOrga:Orga;
    private userInfo:string;
    private secretKey = "amplifynature-2-2019";

    public hideLogin:Boolean = false;

    constructor(
        private translate: TranslateService,
        public router: Router,
        public route: ActivatedRoute,
        private auth: AngularFireAuth,
        private db: AngularFireDatabase,
        public zone: NgZone
        ) {
            console.log("we are here");
            this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
            this.translate.setDefaultLang('fr');
            const browserLang = this.translate.getBrowserLang();
            console.log("Browser lang : %o", browserLang)
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'fr');
    }

    ngOnInit() {
        let self = this;

        this.clientId = this.route.snapshot.paramMap.get("client");
        let userInfo = this.route.snapshot.paramMap.get("userInfo");
        if(userInfo){
            let date = new Date().getDate()+"/"+new Date().getMonth()+"/"+new Date().getFullYear();
            // Decrypt
            var bytes  = CryptoJS.AES.decrypt(this.userInfo, this.secretKey);
            this.userInfo = bytes.toString(CryptoJS.enc.Utf8);

            console.log("sign with token %o", JSON.parse(this.userInfo));
            // this.auth.auth.signInWithCustomToken(this.token).then((user) =>{
            //     self.error = "";
            //     console.log("USER SIGNED : %o", user.user);
            //     db_app2.database().ref("clients").orderByChild("mail").equalTo(user.user.email).once("value", function(snapshot){
            //             console.log("snapshot : ",snapshot.val());
            //         if(snapshot.val()){

            //             for(let i in snapshot.val()){
            //                 let client:Client = new Client().deserialize(snapshot.val()[i]);

            //                 db_app2.database().ref("clients/"+snapshot.val()[i].num+"/last_connection").set(date);

            //                 db_app2.database().ref("orga/"+client.organisation).once("value", (orgaSnap) =>{
            //                     if(orgaSnap.val()){
            //                         let orga:Orga = new Orga().deserialize(orgaSnap.val());
            //                         client.favicon_img_url = orga.favicon_img_url;
            //                         client.frontoffice_img_url = orga.frontoffice_img_url;
            //                         client.orga_img_url = orga.orga_img_url;

            //                         client.flux = orga.flux;


            //                     }
            //                     localStorage.setItem('user', JSON.stringify(client));
            //                     localStorage.setItem('isLoggedin', 'true');

            //                     self.router.navigate(['/home']);
            //                     console.log("user signed %o", client);
            //                     self.loading = false;

            //                 })


            //                 break;
            //             }
            //         }else{
            //             self.router.navigate(['/login']);
            //         }


            //     })
            // })
        }

        this.clientId = this.clientId ? this.clientId.toLowerCase() : null;
        this.background = '';
        console.log("this.clientId %o", this.clientId);
        if(this.clientId && !this.userInfo){
            db_app2.database().ref("clients/"+this.clientId).once("value", function(snapshot){

                console.log("client %o", snapshot.val())
                if(snapshot.val()) {
                    self.client = snapshot.val();

                    let client:Client = new Client().deserialize(snapshot.val());
                    console.log("get orga %o", client.organisation)
                    db_app2.database().ref("orga/"+client.organisation).once("value", (orgaSnap) =>{

                        console.log("orga %o", orgaSnap.val())
                        if(orgaSnap.val()){

                            let orga:Orga = new Orga().deserialize(orgaSnap.val());

                            client.orga_img_url = orga.orga_img_url;
                            client.favicon_img_url = orga.favicon_img_url;
                            client.favicon2_img_url = orga.favicon2_img_url;
                            client.frontoffice_img_url = orga.frontoffice_img_url;

                            // client.flux = orga.flux;

                            self.client = new Client().deserialize(client);

                            let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
                            link.type = 'image/png';
                            link.rel = 'shortcut icon';
                            link.href = orga.favicon2_img_url+"?v=2";

                            window.document.getElementsByTagName('head')[0].appendChild(link);
                            console.log("changing favicon %o %o",orga.favicon_img_url, window.document.getElementsByTagName('head')[0])
                            // console.log("background %o", client.frontoffice_img_url);
                            if(orga.frontoffice_img_url.includes("http")){
                                console.log("background edit %o", orga.frontoffice_img_url);

                                self.background = orga.frontoffice_img_url;
                                console.log("STYLE %o", self.loginStyle);
                            }

                        }
                    })


                }
            }).catch((err) =>{
                console.log("Erreur : %o", JSON.parse(err));
            })
        }else if(!this.clientId && !this.userInfo){
            db_app2.database().ref("orga").once("value",function(data){
                for(let i in data.val()){
                    let user:Orga  = new Orga().deserialize(data.val()[i]);
                    console.log('user ultime ',user);
                    if(user.favicon2_img_url){
                        let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
                        console.log("first link %o", link);
                        link.type = 'image/svg+xml';
                        link.rel = 'shortcut icon';
                        link.href = user.favicon2_img_url+"?v=8989";
                        console.log("Changing favicon to %o", link.href);

                        window.document.getElementsByTagName('head')[0].appendChild(link);
                        break;
                    }

                }
            });
        }

        this.route.params.subscribe((params: Params) => {
           if (params.email){
               let email_decrypt = CryptoJS.AES.decrypt(params.email, "crypt");
               // console.log(email_decrypt);
                this.email = email_decrypt.toString(CryptoJS.enc.Utf8).split("__")[0];
                this.password = email_decrypt.toString(CryptoJS.enc.Utf8).split("__")[1];
                console.log(this.email);
                this.getLoginParams(this.email, this.password);
            } else if(params.resetEmail && params.reset){
                this.email = params.resetEmail;
                this.loginPage = false;
                this.resetPassword();
            }
           // if (params.resetEmail){
           //      this.email = params.resetEmail;
           //      this.loginPage = false;
           //          //this.resetPassword(;
           //  }
        });


    }

    getLoginParams(email, password){
        this.email = email;
        this.password = password;
        this.hideLogin = true;
        this.loading = true;
        this.onLoggedin();
    }

    onLoggedin() {
        let self = this;
        this.loading = true;
        console.log(this.email );
        console.log(this.password );
        let data = new Date().getDate()+"/"+new Date().getMonth()+"/"+new Date().getFullYear();
        if(this.email && this.password){
            this.auth.auth.signInWithEmailAndPassword(this.email, this.password).then(function(user){
                self.error = "";
                db_app2.database().ref("clients").orderByChild("mail").equalTo(self.email).once("value", function(snapshot){
                        console.log("snapshot : ",snapshot.val());
                    if(snapshot.val()){

                        for(let i in snapshot.val()){
                            let client:Client = new Client().deserialize(snapshot.val()[i]);

                            db_app2.database().ref("clientsLast/"+snapshot.val()[i].num).set(data);

                            console.log("client.orga %o", client.orga);
                            self.client = client;
                            if(client.orga.length > 1){
                                console.log("getting orga");

                                db_app2.database().ref("orga").once("value", (orgaSnap) =>{
                                    self.zone.run(() =>{

                                        self.loading = false;
                                        // self.chooseOrga = true;
                                        self.chooseOrga = false; // supprimer page choix des orga direct accor-hotels
                                        self.loginPage = false;
                                        // if(orgaSnap.val()){
                                            // let orga:Orga = new Orga().deserialize(orgaSnap.val());
                                            // client.favicon_img_url = orga.favicon_img_url;
                                            // client.frontoffice_img_url = orga.frontoffice_img_url;
                                            // client.orga_img_url = orga.orga_img_url;

                                            // client.flux = orga.flux;


                                        // }

                                        orgaSnap.forEach((orgaSnapChild) =>{
                                            let orga:Orga = new Orga().deserialize(orgaSnapChild.val());
                                            orga.key = orgaSnapChild.key;
                                            if(client.orga.find((el) => el == orgaSnapChild.key)) {
                                                self.orga.push(orga);
                                                // console.log("orga found orga %o", orga);
                                            }
                                        });
                                        self.curOrga = self.slug(client.organisation);
                                        // self.selectedOrga = self.orga.find((el) => el.key == self.curOrga);
                                        console.log('tout les orga du client = ',self.orga);
                                        self.selectedOrga = self.orga.find((el) => el.key == 'eiffage');
                                        self.selectOrga();
                                        // client.favicon_img_url = self.orga[0].favicon_img_url;
                                        // client.frontoffice_img_url = self.orga[0].frontoffice_img_url;
                                        // client.orga_img_url = self.orga[0].orga_img_url;

                                        // client.flux = self.orga[0].flux;

                                        // localStorage.setItem('user', JSON.stringify(client));
                                        // localStorage.setItem('isLoggedin', 'true');

                                        // choosing organisation
                                        // if(client.orga.length > 0){
                                        //     self.chooseOrga = true;
                                        //     self.loginPage = false;
                                        // }else{
                                        //     self.router.navigate(['/home']);
                                        //     console.log("user signed %o", client);
                                        //     self.loading = false;
                                        // }


                                    })



                                })
                            }else if(client.orga.length == 1){
                                db_app2.database().ref("orga/"+client.orga[0]).once("value", (orgaSnap) =>{
                                    let orga = new Orga().deserialize(orgaSnap.val());
                                    self.selectedOrga = orga;
                                    self.selectOrga();
                                })
                            }



                            break;
                        }
                    }else{
                        self.router.navigate(['/login']);
                        self.loading = false;
                    }


                })


            }).catch(function(error) {
                  self.error = error;
                  self.loading = false;
            });
        }else{
            this.loading = false;
        }
    }

    public onChangeOrga(){
        this.selectedOrga = this.orga.find((el) => el.key == this.curOrga);
    }

    public selectOrga(){
        console.log("this.selectedOrga",this.selectedOrga);
        this.client.favicon_img_url = this.selectedOrga.favicon2_img_url;
        this.client.frontoffice_img_url = this.selectedOrga.frontoffice_img_url;
        this.client.orga_img_url = this.selectedOrga.orga_img_url;

        // this.client.flux = this.selectedOrga.flux;
        this.client.organisation = this.selectedOrga.key;

        localStorage.setItem('user', JSON.stringify(this.client));
        localStorage.setItem('orga', JSON.stringify(this.selectedOrga));
        localStorage.setItem('isLoggedin', 'true');
        this.router.navigate(['/home']);

    }


    public slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        let from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        let to   = "aaaaaeeeeeiiiiooooouuuunc------";
        for (let i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

        return str;
      };

    forgotPassword() {
        this.loginPage = false;
    }
    login() {
        this.loginPage = true;
    }

    resetPassword() {
        const self = this;
        if (this.email) {
            this.loading = true;
            this.auth.auth.sendPasswordResetEmail(this.email).then(function() {
                self.error = "";
                self.loginPage = true;
                self.loading = false;
            }).catch(function(error) {
                  self.error = error;
                  self.loading = false;
            });
        }
    }
}
