import { EChartOption } from 'echarts';

declare interface _Format extends EChartOption.Tooltip.Format {
  marker: string;
  axisValueLabel: string;
}

const formatter: EChartOption.Tooltip.Formatter = function(params: _Format|Array<_Format>, _ticket, _callback) {
  if(params instanceof Array) {
    if(params.length) {
      let message = '';
      message += `${ params[0].axisValueLabel }` + " "+ `${ params[0].data.year }`;
      params.forEach(param => {
     
        message += `<br/>${ param.marker }${ param.seriesName }: ${ param.value }${  param.data.unit+(param.value > 1 ? 's' : '') || '' }`;
      });
      return message;
    } else {
      return null;
    }
  } else {
    let message = '';
    // message += `${ params[0].axisValueLabel }`;
    message += `${ params.marker }${ params.seriesName } ${ params.data.year } : ${ params.value }${ params.data.unit+(params.value > 1 ? 's' : '') || '' }`;
    return message;
  }
};

export default formatter;