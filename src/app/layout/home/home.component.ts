import { environment } from "../../../environments/environment";
import * as mapboxgl from "mapbox-gl";
import { AngularFireDatabase } from "@angular/fire/database";
import { AngularFireAuth } from "@angular/fire/auth";
import { Enjeu } from "../../shared/models/enjeu.model";
import { Phrase } from "../../shared/models/phrase.model";
import { CsvPhrase } from "../../shared/models/csv-phrase.model";
import { Client, Orga } from "../../shared/models/client.model";

import * as jQuery from "jquery";

import { Component, Output, EventEmitter, OnInit, ViewEncapsulation, NgZone, ViewChild, ElementRef } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import * as echarts from "echarts";

import { NgbModal, ModalDismissReasons, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import formatter from "./tooltip-formatter";
import { EChartOption } from "echarts";

import { Options, ChangeContext, PointerType } from "ng5-slider";
import { Flux } from "src/app/shared/models/flux.model";
import { DocumentService } from "src/app/shared/services/document.service";
import { connectableObservableDescriptor } from "rxjs/internal/observable/ConnectableObservable";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
// import { BrowserModule } from '@angular/platform-browser';
// import { FormsModule } from '@angular/forms';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';




@Component({
	// encapsulation: ViewEncapsulation.None,
	selector: "app-home",
	templateUrl: "./home.component.html",
	styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {

	@ViewChild("webpageModal") public webpageModal;
	@ViewChild("warningaccess") warningaccess: any;
	@ViewChild("fichepdg") fichepdg: any;

	value = 0;
	options: Options = {};
	highValue = 0;

	enjeux: Enjeu[];
	enjeux2: Enjeu[];
	allEnjeux: Enjeu[];
	selectedEnjeux: Enjeu[] = [];
	enjeuxExtraits: { [key: string]: number };
	enjeu: Enjeu;
	enjeuOver: Enjeu;
	enjeuInfo: Enjeu;
	seuil = 0;
	csvPhrases: CsvPhrase[];
	csvPhrasesFiltered: CsvPhrase[];
	csvPhrasesFilteredMap: CsvPhrase[];
	csvPhrase: CsvPhrase = new CsvPhrase();
	csvPhrase2: CsvPhrase = new CsvPhrase();
	csvSelected: string;

	phrases: any = {};

	loading: Boolean = false;
	loadingMap: Boolean = false;
	phrasesLoading: Boolean = false;
	traduction: any = {};
	couleurs: any = {};
	dates: string[] = [];
	years: string[] = [];
	pays: string[] = [];
	paysFilter = "all";

	couleurPhrase: any = {};

	couleur_annee_existant = "";
	couleur_annee_vide = "";
	couleur_annee_defaut = "";
	data_slider: any = [];

	allFlux: any[] = [];
	fluxFilter: any[] = [];
	perfFilter: any[] = [];

	extraits: any[] = [];

	allFluxFilter = true;
	allPerf: Boolean = true;
	fluxLimit: Number = 3;

	yearFilter: any = false;
	month_from: string;
	year_from: string;

	month_to: string;
	year_to: string;

	fluxData: any[] = [];
	csvPhrasesData: any[] = [];


	mapToogle = true;
	collapedSideBar: boolean;

	isActive: boolean;
	isActive2: boolean;
	isActive3: boolean;
	collapsed: boolean;
	collapsed2: boolean;
	collapsed3: boolean;
	showMenu: string;
	pushRightClass: string;
	pushLeftClass: string;

	client: Client = new Client();

	chartOptions: any;
	chartOptions2: any;
	closeResult: string;

	user: Client;
	mondeFlux: Flux[];

	sideEnjeux = false;
	client_enjeu: Enjeu[] = [];
	titre_fr: any[] = [];
	titre_en: any[] = [];
	phrase_en: any[] = [];
	phrase_fr: any[] = [];
	mot_cle: any[] = [];
	countEnjeu: any;
	motEntree = "";
	motEntree2 = "";
	motEntree3 = "";
	allPaysFilter: Boolean = true;
	newpaysFilter: any[] = [];
	filter_enjeu: any[] = [];
	filter: any;
	hide_map = true;
	hide_graph = false;
	showFilterDate = false;
	showSliderDate = false;
	showRangeDate = false;
	csvClick = false;
	next: number;
	showCheck = false;
	liste_pays: any[] = [];
	all_pays = "Indifférent";
	dateFilterToggle: boolean;
	showSlider = true;
	test = false;
	test2 = false;

	countExtraits: any[] = [];

	satCsv: any = {};

	orga: Orga;
	orgas: Orga[];

	motCle = "";
	motCle2 = "";
	motCle3 = "";
	selectedMotsCle: any[] = [];

	statMotCle1:boolean = true;
	statMotCle2:boolean = false;
	statMotCle3:boolean = false;
	checkMotcle = false;
	borderMotcle = "";

	generateChart = false;

	nbExtraits = 0;
	pageLink: string;

	categories: string[];
	categorySelected: string;

	edit = false;
	orgas2: Orga[];
	hideEnjeu = false;
	mondeSelected = true;
	maxMonde = 0;

	fluxExtraits: any;

	@Output() collapsedEvent = new EventEmitter<boolean>();

	allOrgaImageUrl: any = {};

	checkMotCleExist = "";
	showFilterIcon:boolean = false;
	showEyeIcon:boolean = false;
	showEtiquetteGrap: boolean = false;
	filterCat:boolean = true;
	svgs:{[key: string]: SafeHtml} = {}
	constructor(
		private translate: TranslateService, public router: Router,
		private db: AngularFireDatabase,
		private auth: AngularFireAuth,
		private modalService: NgbModal,
		private zone: NgZone,
		private documentService: DocumentService,
		private sanitizer: DomSanitizer

	) {
		this.translate.addLangs(["en", "fr", "ur", "es", "it", "fa", "de"]);
		this.translate.setDefaultLang("fr");
		// const browserLang = this.translate.getBrowserLang();
		// this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'fr');
		const user: Client = JSON.parse(localStorage.getItem("user"));

		this.translate.use(user && user.langue ? user.langue.toLowerCase() : "fr");
		this.router.events.subscribe(val => {
			if (
				val instanceof NavigationEnd &&
				window.innerWidth <= 992 &&
				this.isToggled()
			) {
				this.toggleSidebar();
			}
		});


		this.csvPhrase.phrases = [];
		this.couleurPhrase = { "0": "#eee", "1": "#D8F0BC", "2": "#AEE571", "3": "#7CB342", "4": "#4B830D" };
	}

	async ngOnInit() {

			

		const self = this;

		this.user = new Client().deserialize(JSON.parse(localStorage.getItem("user")));

		let orga = JSON.parse(localStorage.getItem('orga'));

		if (orga) {
			this.orga = new Orga().deserialize(orga);
			this.db.database.ref("orga/" + this.orga.key).on("value", (snap) => {
				if (snap.exists()) {
					orga = new Orga().deserialize(snap.val());
					this.orga = orga;
					// console.log("ORGA %o", orga);
					// const link = (document.querySelector("link[rel*='icon']") || document.createElement("link")) as HTMLLinkElement;
					// link.type = "image/png";
					// link.rel = "shortcut icon";
					// link.href = orga.favicon2_img_url + "?v=" + Date.now();
					// console.log('favicon discours = ',link.href)
					// window.document.getElementsByTagName("head")[0].appendChild(link);
				}
			})
			this.db.database.ref("clientsPng/favicon_2").on("value", (snap) => {
				if (snap.exists()) {
					let favicon = snap.val();
					let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
					link.type = 'image/png';
					link.rel = 'shortcut icon';
					link.href = favicon.url + "?v=" + Date.now();
					console.log('nous somme ici dans ')
					window.document.getElementsByTagName('head')[0].appendChild(link);
				
				}
			})
		} else {
			this.deconnexion();
		}

		// console.log("orgs %o", orga);

		jQuery(".main-container").css("margin-left", "0");



		this.loading = true;
		this.loadingMap = true;


		this.db.database.ref("trad").once("value", function (data) {
			data.forEach(function (child) {
				self.traduction[child.val().ref.toLowerCase().replace(/\s+$/, "")] = child.val();
			});
			// console.log("self.traduction %o", self.traduction)

		});
		this.db.database.ref("couleur").once("value", function (data) {
			// self.couleurs = data.val();
			for (const i in data.val()) {
				self.couleurs[data.val()[i].objet.toLowerCase().trim()] = data.val()[i].couleur;
				if(data.val()[i].objet.trim() == "couleur_annee_existant") {
					self.couleur_annee_existant = data.val()[i].couleur;
				}
				if(data.val()[i].objet.trim() == "couleur_annee_vide") {
					self.couleur_annee_vide = data.val()[i].couleur;
				}
				if(data.val()[i].objet.trim() == "counleur_annee_defaut"){
					self.couleur_annee_defaut = data.val()[i].couleur;
				}
			}
			self.couleurPhrase = { "0": "#eee", "1": self.getColor('très négatif'), "2": self.getColor('déceptif'), "3": self.getColor('encourageant'), "4": self.getColor('très positif'), "-1": self.getColor('Objectif non traité') };
		});
		this.db.database.ref("enjeux_2_extraits").once('value', (data) => {
			this.enjeuxExtraits = {}
			data.forEach((child) => {
				this.enjeuxExtraits[child.key] = child.val();
			});

		});


		// this.db.database.ref("enjeux_extraits").once('value', function(data){

		//      self.enjeuxExtraits = data.val();

		//   })
		// this.db.database.ref("enjeux_extraits").once('value', function(data){
		// 	// for(let i in data.val()){
		// 	// 	console.log(i);
		// 	// 	console.log(data.val()[i]);

		// 	// }
		// 	data.forEach(function(child){
		// 		let count = 0;
		// 		//console.log("ID",child.key);
		// 		for(let i in child.val()){
		// 			count = count + child.val()[i];

		// 		}
		// 		self.countExtraits.push({"id":child.key,"count": count});
		// 		//console.log(child.key," : ",count);
		// 		self.countExtraits = self.countExtraits.sort(function(a, b){return parseInt(b.count)-parseInt(a.count)});
		// 	});
		// 	//console.log("LES EXTRAITS : ", self.countExtraits);
		// });

		const refEnjeu = self.db.list("enjeu").snapshotChanges().subscribe(function (data) {
			refEnjeu.unsubscribe();

			const enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));

			self.allEnjeux = enjeux.map((a: Enjeu) => new Enjeu().deserialize(a));
			// console.log("self.allEnjeux %o",self.allEnjeux)
		});




		self.db.database.ref("clients/" + self.user.num).on("value", (data) => {
			// console.log("clients subscribe");
			self.motCle = data.val() ? data.val().mots_cles : "";
			self.motCle2 = data.val() ? data.val().mots_cles2 : "";
			self.motCle3 = data.val() ? data.val().mots_cles3 : "";
			
			if(self.motCle) self.selectedMotsCle.push(self.motCle.trim());
			// if(self.motCle2) self.selectedMotsCle.push(self.motCle2.trim());
			// if(self.motCle3) self.selectedMotsCle.push(self.motCle3.trim());

			const client = new Client().deserialize(data.val());
			self.db.database.ref("clientFluxSelected/" + self.orga.key + "/" + self.user.num).once("value", (data2) => {
				// console.log("clientFluxSelected subscribe");
				client.fluxSelected = []
				data2.forEach((child) => {
					client.fluxSelected.push(new Flux().deserialize(child.val()))
				})

				// console.log("ORGA FLUX %o",self.orga.flux);
				// client.flux = self.orga.flux;
				client.orga_img_url = self.orga.orga_img_url;
				self.user = client;
				self.allFluxFilter = true;

				// let flux: Flux = new Flux();
				// flux.flux = "Avril (Groupe)";
				// this.user.flux.push(flux);

				const orgas: any = [];
				orgas.push(this.orga.organisation);
				for (const org in this.orga.concurrents) {
					if (this.orga.concurrents[org] != "") {
						orgas.push(this.orga.concurrents[org]);
					}
				}

				console.log("org in orgas", orgas);
				// let temp = orgas;
				this.user.flux = [];
				const index = 0;
				for (const org in orgas) {
					console.log("org in orgas", orgas[org]);
					const flux: Flux = new Flux();
					flux.flux = orgas[org];
					this.user.flux.push(flux);
				}

				// console.log("ORGAAAS %o",orgas);
				// console.log("FLUX USERS %o",this.user.flux);

				client.fluxSelected.forEach((flux) => {
					// console.log("ID FLUX %o",flux.id);
					if (self.user.flux[flux.id]) {
						self.user.flux[flux.id].selected = true;
					}

				});

				this.db.database.ref("orga").on("value", (snap) => {
					console.log("orga subscribe");
					this.orgas = [];
					snap.forEach((child) => {
						const o: Orga = new Orga().deserialize(child.val());
						o.key = child.key;
						this.orgas.push(o);
					});
					let index = 0;
					let monde = false;
					let mondeSelected = true;
					this.mondeFlux = [];
					for (const o of this.orgas) {
						if (o && o.type.trim().toLowerCase() == "t4") {
							// console.log("MONDE");
							const flux: Flux = new Flux();
							flux.flux = o.organisation;
							flux.isMonde = true;
							flux.selected = true;
							this.user.flux.push(flux);
						}
						const orga_formatted = o.organisation.toString().toLowerCase().trim().replace(/\s/g, "");
						this.allOrgaImageUrl[orga_formatted] = o.orga_img_url;
					}

					for (const flux of this.user.flux) {
						const o: Orga = this.orgas.find((e) => this.slug(flux.flux) == e.key);
						// console.log("orga type", o);
						if (o && o.type.trim().toLowerCase() == "t4") {
							// console.log("MONDE");
							this.user.flux[index].isMonde = true;
							this.mondeFlux.push(flux);
							monde = true;
							if (this.user.flux[index].selected) {
								mondeSelected = true;
								this.mondeSelected = true;
							}
						}
						if (!flux.selected) {
							this.allFluxFilter = false;
						}
						index++;
					}

					if (monde) {
						if (!this.user.flux.find((f) => f.flux.trim().toLowerCase() == "monde")) {
							const flux: Flux = new Flux();
							flux.flux = "Monde";
							flux.group = true;
							flux.selected = this.allFluxFilter || mondeSelected;
							this.user.flux.push(flux);
						}

					}

					// let ref = this.documentService.listDocuments().subscribe((data) =>{
					const ref = this.documentService.listDocumentsByOrga(this.orga.key).subscribe((data) => {
						ref.unsubscribe();

						self.dates = [];
						self.pays = [];
						self.years = [];
						const prev = [];
						// console.log("data", data)
						self.csvPhrases = data.map((a: any) => {
							return new CsvPhrase().deserialize(a);
						});

						self.user.flux = self.user.flux ? self.user.flux.filter((v, i) => self.user.flux.indexOf(v) === i) : [];
						// self.flux = self.user.flux;
						// console.log("self.csvPhrases %o",self.csvPhrases);
						// console.log("self.csvPhrases 597 %o",self.csvPhrases);
						// console.log("self.user.flux %o",self.user.flux);
						// checkFluxUser(flux: string, cons = false) {
						// 	let res = false;
						self.csvPhrases = self.csvPhrases.filter(function (item) {

							return self.checkFluxUser(item.flux);


						});
						self.csvPhrases = self.csvPhrases.map((a: CsvPhrase) => {
							self.dates.push(a.date);
							self.pays.push(a.pays);
							// bug 1
							self.phrases[a.nom] = a.phrases;

							const dateCsvPhr = new Date(a.date);	
							if (dateCsvPhr.getFullYear() >= 2000) {
								self.years.push(''+dateCsvPhr.getFullYear());
							}
	

							return a;
						});
						self.csvPhrasesFiltered = self.csvPhrases;
						// console.log("self.csvPhrases %o",self.csvPhrases);


						jQuery(".main-container").css("margin-left", "342px");
						self.db.database.ref("clientEnjeux/" + self.user.num + "/client_enjeu").once("value", function (data) {
							const enjeu: Enjeu = new Enjeu();
							if (data.val()) {
								const enjeux = data.val();
								self.selectedEnjeux = [];
								for (const enj of enjeux) {
									if (enj.enjeu_big == self.categorySelected) {
										self.selectedEnjeux.push(new Enjeu().deserialize(enj));
									}
								}
								if (!self.client_enjeu || (self.client_enjeu && self.client_enjeu.length == 0)) {
									self.selectedEnjeux = [];
								}


							} else {
								self.selectedEnjeux = [];
								if (self.client_enjeu.length > 0) {
									for (const enj of self.client_enjeu) {
										if (enj.enjeu_big == self.categorySelected) {
											self.selectedEnjeux.push(self.client_enjeu[0]);
											break;
										}
									}

								}
							}
							// console.log("self.selectedEnjeux %o", self.selectedEnjeux);


							// self.checkFilter();
							self.zone.run(() => {
								self.loading = false;
								self.loadingMap = false;
							})


							if (self.categories.length > 0) {
								self.selectCategory(self.categories[0]);
							}


						});


						self.years = self.years.filter((v, i) => self.years.indexOf(v) === i);
						self.pays = self.pays.filter((v, i) => self.pays.indexOf(v) === i);
						self.years = self.years.sort(function (a, b) { return parseInt(a) - parseInt(b) });

						self.checkTimelapse();




					});




				});


			});




		});

		this.selectTab("map");


		const ref = self.db.list("enjeu_2").snapshotChanges().subscribe((data) => {
			ref.unsubscribe();
			this.enjeux2 = [];
			data.forEach((child) => {
				const enj: Enjeu = new Enjeu().deserialize(child.payload.val());
				enj.key = child.key;
				this.enjeux2.push(enj);
			});
			// console.log("ENJEUUUX 2 %o", this.enjeux2);

		});
		const refEnjeu2 = self.db.list("enjeu").snapshotChanges().subscribe((data) => {
			refEnjeu2.unsubscribe();
			this.enjeux = [];
			this.categories = [];
			data.forEach((child) => {
				const enj: Enjeu = new Enjeu().deserialize(child.payload.val());
				enj.key = child.key;
				this.enjeux.push(enj);
				if (!this.categories.includes(enj.enjeu_big)) {
					this.categories.push(enj.enjeu_big);
				}
				/*console.log(this.categories);*/
			});
			// console.log(this.enjeux);
			// console.log(this.categories);
			if (this.categories.length > 0) {
				this.categorySelected = this.categories[0];
				for (const enj of this.enjeux) {
					if (enj.enjeu_big == this.categorySelected) {

					}
				}
			}

			this.documentService.countMondeMaxDocs().subscribe((mondeMaxDocsData) => {
				this.maxMonde = mondeMaxDocsData.data.max;
				for (const i in mondeMaxDocsData.data.extraits) {
					this.countExtraits.push({ id: i, count: mondeMaxDocsData.data.extraits[i] });
				}



				// console.log("this.countExtraits", this.countExtraits);



				self.db.list("clientEnjeux/" + self.user.num + "/enjeu").snapshotChanges().subscribe(async (data) => {

					console.log("ENJEUX = ", self.enjeux);
					self.getPictoSvg();
					const client_enjeu: any = data.map(a => ({ cle: a.key, selected: false, ...a.payload.val() }));
					self.client_enjeu = client_enjeu.filter(function (a: any) {
						a.id = "enjeu" + a.num.replace(/#/g, "");
						const result = self.countExtraits.find((elt) => elt.id == a.key);

						const enj = self.enjeux.find((element) => element.num == a.num);
						if(enj){
							enj.count = result ? result.count : 0;
							return enj;
						}
					});
					self.fluxExtraits = [];
					const promises = [];
					const fluxMonde = self.user.flux.filter((f) => f.isMonde).map((f) => f.flux);
					self.user.flux.forEach((flux) => {
						if (!flux.isMonde) {
							self.client_enjeu.forEach((enj) => {
								if (self.fluxExtraits[flux.flux] == undefined) {
									self.fluxExtraits[flux.flux] = [];
								}
								let fluxArr = [];
								if (flux.flux.toLowerCase() == "monde") {
									fluxArr = fluxMonde;
								} else {
									fluxArr = [flux.flux];
								}
								promises.push(
									this.documentService.getEnjeuExtraits(enj.num, fluxArr).subscribe((data) => {
										self.fluxExtraits[flux.flux][enj.num] = data;
									})
								);
							});
						}


					});


					await Promise.all(promises);


					// // sort enjeux
					// this.documentService.getEnjeuxWithExtraits().subscribe(result => {

					// 	self.client_enjeu.sort(function (a, b) {
					// 		let currentEnjeuA = result.find((elt) => elt.num_enjeu == a.num);
					// 		let currentEnjeuB = result.find((elt) => elt.num_enjeu == b.num);
					// 		return parseInt(currentEnjeuB.extraits_enjeu) - parseInt(currentEnjeuA.extraits_enjeu)
					// 	});

					// });

					self.sortEnjeux();

					self.countEnjeu = self.client_enjeu.length;
					if (self.countEnjeu === 15) {
						self.closeEnjeux();
					}

					for (const cli of self.enjeux) {
						const list = self.client_enjeu.filter(x => x.key === cli.key);
						const removeIndex = self.client_enjeu.findIndex(itm => itm.key === cli.key);
						if (removeIndex !== -1) {
							self.client_enjeu.splice(removeIndex, 1);
						}
						list.forEach(x => {
							self.client_enjeu.push(x);
						});
					}

					/*if (this.categories.length > 0) {
						this.selectCategory(this.categories[0]);
					}*/

					// self.client_enjeu.sort((a, b) => b.count - a.count)

					// sort enjeux to add
					// self.enjeux.map((enjeu) => {
					// 	let res = self.countExtraits.find((elt) => elt.id == enjeu.key);
					// 	enjeu.count = res ? res : 0;
					// })
					// self.enjeux.sort((a, b) => b.count - a.count)

					// console.log("self.client_enjeu %o",self.client_enjeu )
					// console.log("self.countExtraits %o",self.countExtraits )

				});
			});

		});

		self.db.database.ref("enjeux_extraits").once("value", function (extraitsData) {
			self.extraits = extraitsData.val();
		});
		if(this.user.app_1.trim().toLowerCase() !== "discours" && this.user.app_2.trim().toLowerCase() !== "discours"){
			setTimeout(()=> {
				this.open(this.warningaccess);
			}, 0);
		}
	}

	sortEnjeux() {
		this.client_enjeu.map((enj) => {
			enj.count = 0;
			return enj;
		});
		this.user.flux.forEach((f) => {
			if (f.selected && !f.isMonde) {
				this.client_enjeu.map((enj) => {
					enj.count += this.fluxExtraits[f.flux][enj.num] ? this.fluxExtraits[f.flux][enj.num] : 0;
					return enj;
				});
			}
		});
		this.client_enjeu.sort((a, b) => b.count - a.count);
	}

	selectCategory(cat: string) {
		this.hideEnjeu = false;
		this.categorySelected = cat;
		/*console.log("this.client_enjeu : ", this.client_enjeu);
		console.log("this.categorySelected : ", this.categorySelected);
		console.log("this.selectedEnjeux[0] : ", this.selectedEnjeux[0]);*/
		if (this.selectedEnjeux[0] && this.selectedEnjeux[0].enjeu_big !== this.categorySelected) {
			// console.log("ato 1");
			this.selectedEnjeux = [];
			// console.log(this.client_enjeu);
			/*for (let enj of this.client_enjeu) {*/
			for (const enj of this.enjeux) {
				// console.log(" 1 - enj",enj);
				if (enj.enjeu_big === this.categorySelected) {
					// console.log("1 - enj selectionné ",enj);
					this.selectedEnjeux.push(enj);
					// this.selectEnjeu(enj, null);
					// break;
				}
			}

			this.selectAllEnjeu();

		} else {
			// console.log("ato 2");
			/*for (let enj of this.client_enjeu) {*/
			for (const enj of this.enjeux) {
				// console.log(" 2 - enj",enj);
				if (enj.enjeu_big === this.categorySelected) {
					// console.log("2 - enj selectionné ",enj);
					this.selectedEnjeux.push(enj);
					// this.selectEnjeu(enj, null);
					// break;
				}
			}

			this.selectAllEnjeu();
		}
		(this.mapToogle === true && this.hide_graph === true && this.hide_map === false) && this.selectTab("chart2");
	}

	getClientEnjeuxNumber() {
		let res = 0;
		// console.log("this.client_enjeu", this.client_enjeu);
		if (this.client_enjeu !== undefined) {
			for (const enj of this.client_enjeu) {
				if (enj.enjeu_big === this.categorySelected) {
					res++;
				}
			}
		}

		return res;
	}

	getAllEnjeuxNumber() {
		let res = 0;
		for (const enj of this.enjeux) {
			if (enj.enjeu_big === this.categorySelected) {
				res++;
			}
		}
		return res;
	}

	chartClick(chart) {
		const page1 = this.generateChartType(1);
		const page2 = this.generateChartType(2);
		// console.log(page1);
		this.db.database.ref("chartPages/" + this.orga.key + "/2/" + page1.date).set(page1).then(() => {

			this.db.database.ref("chartPages/" + this.orga.key + "/22/" + page1.date).set(page2).then(() => {

				this.loading = false;
				this.pageLink = "https://amplifynature.com/graphique/" + this.orga.key + "/2/" + page1.date;
				window.open(this.pageLink, "_blank");
			});
		});


	}

	generateChartType(filtre = 1) {
		this.loading = true;
		const chartOption: EChartOption = {
			color: [
				"#2ec7c9", "#b6a2de", "#5ab1ef", "#ffb980", "#d87a80",
				"#8d98b3", "#e5cf0d", "#97b552", "#95706d", "#dc69aa",
				"#07a2a4", "#9a7fd1", "#588dd5", "#f5994e", "#c05050",
				"#59678c", "#c9ab00", "#7eb00a", "#6f5553", "#c14089",

				"#C1232B", "#27727B", "#FCCE10", "#E87C25", "#B5C334",
				"#FE8463", "#9BCA63", "#FAD860", "#F3A43B", "#60C0DD",
				"#D7504B", "#C6E579", "#F4E001", "#F0805A", "#26C0C0",

				"#E01F54", "#001852", "#f5e8c8", "#b8d2c7", "#c6b38e",
				"#a4d8c2", "#f3d999", "#d3758f", "#dcc392", "#2e4783",
				"#82b6e9", "#ff6347", "#a092f1", "#0a915d", "#eaf889",
				"#6699FF", "#ff6666", "#3cb371", "#d5b158", "#38b6b6"
			],
			grid: {
				left: 200,
				right: "5%",
				bottom: "3%",
				// top : "40%",
				containLabel: true
			},
			xAxis: {
				type: "value",
				axisLabel: {
					formatter: function (value, index) {
						return value + " extrait" + (value > 0 ? "s" : "");
					}
				}
			}

		};

		const yAxisData = [];

		chartOption.tooltip = {};
		chartOption.tooltip["type"] = "item";
		chartOption.tooltip["formatter"] = formatter;

		const legendData = [];
		const legendData_2 = this.enjeux2.map((enj) => (this.translate.currentLang == 'fr') ? this.fr(enj.titre) : this.en(enj.titre));
		const resEmetteur: any = {};
		chartOption.series = [];

		let iteration = 0;
		const colors = [
			"#2ec7c9", "#b6a2de", "#5ab1ef", "#ffb980", "#d87a80",
			"#8d98b3", "#e5cf0d", "#97b552", "#95706d", "#dc69aa",
			"#07a2a4", "#9a7fd1", "#588dd5", "#f5994e", "#c05050",
			"#59678c", "#c9ab00", "#7eb00a", "#6f5553", "#c14089"
		];
		this.nbExtraits = 0;

		if (this.hideEnjeu) {
			console.log("first : ", legendData, this.motCle);
			if (!legendData.includes(this.motCle)) {
				legendData.push(this.motCle);
			}
		}

		let monde = false;
		const enjeuxArray = (filtre === 1) ? this.selectedEnjeux : this.enjeux2;
		// console.log('generateChartType',this.selectedEnjeux);
		// console.log(this.enjeux2);
		// console.log(enjeuxArray);
		const csvData = [];
		const csvMonde = [];
		const extraitsUncat = [];
		let extraitsUncat2 = [];
			for (const i in this.user.flux) {
				let ok = false;
				if (this.user.flux[i].isMonde && this.mondeSelected) {
					ok = true;
				}
				if(ok){
					const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
						let res = false;
						if (this.slug(a.flux) === this.slug(this.user.flux[i].flux)) {
							res = true;
						}
						return res;
					});
					for(const idenj in enjeuxArray) {
						for (const c in csvPhrases) {
							if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
								if(filtre === 1 ){
									if (enjeuxArray[idenj] && csvPhrases[c].enjeu_2_color && csvPhrases[c].enjeu_2_color["#" + enjeuxArray[idenj].key]) {}
									else if (!this.checkMotcle) {
										continue;
									}
									else {
										
										if (csvPhrases[c].nom === this.csvSelected) {
											
										}
									}
									if (csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom && csv.enjeu === enjeuxArray[idenj].key)) {
										continue;
									}
									csvData.push({nomCsv: csvPhrases[c].nom , enjeu:enjeuxArray[idenj].key});
									let phrasesActif = [];
									let phrasesUncategorized = [];
									this.phrases[csvPhrases[c].nom].map(
										(value) => {
											// if ("#" + enjeuxArray[idenj].num === value.enjeu){
											if (enjeuxArray[idenj].num === value.enjeu){
												phrasesActif.push(value);
												if(value.enjeu_2_color !== 'uncategorized' ){
												}
												else{
													phrasesUncategorized.push(value);
												}
											}
										}
									)
									let dateCsv = new Date(csvPhrases[c].date);
									let anneeCsv = ''+dateCsv.getFullYear(); 
									
									csvMonde.push(
										{
											enjeu: enjeuxArray[idenj].key,
											year: anneeCsv, 
											// year: csvPhrases[c].date.split('/')[2], 
											csvnom: csvPhrases[c].nom,
											csv: csvPhrases[c],
											phrases: phrasesActif,
											phrases2: phrasesUncategorized
										}
									);
								}
								if(filtre === 2){
									if(Object.keys(csvPhrases[c].enjeu_2_color).length !== 0){
										for(const enjeu in csvPhrases[c].enjeu_2_color){
											if (this.selectedEnjeux.find((enj) => enj.num === enjeu)) {
												if(csvPhrases[c].enjeu_2_color[enjeu] ===  enjeuxArray[idenj].num){
													if (csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom && csv.enjeu === enjeuxArray[idenj].key && csv.enjeuOther === enjeu)) {
														continue;
													}
													csvData.push({nomCsv: csvPhrases[c].nom , enjeu:enjeuxArray[idenj].key, enjeuOther:enjeu});
													let phrasesActif = [];
													this.phrases[csvPhrases[c].nom].map(
														(value) => {
															if ("#" + enjeu === value.enjeu){
																if(value.enjeu_2_color !== 'uncategorized' ){
																	phrasesActif.push(value);
																}
															}
														}
													)
													let dateCsv = new Date(csvPhrases[c].date);
													let anneeCsv = ''+dateCsv.getFullYear(); 
													csvMonde.push(
														{
															enjeu: enjeuxArray[idenj].key,
															enjeuOther: enjeu,
															year: anneeCsv, 
															// year: csvPhrases[c].date.split('/')[2], 
															csvnom: csvPhrases[c].nom,
															csv: csvPhrases[c],
															phrases: phrasesActif
														}
													)
												}
											}
										}
									}
								}
								
							}
						}
					}
				}
			}
		if(filtre === 1){
			csvMonde.map(
				(c) => {
					if(extraitsUncat.find( ext => ext.year === c.year )){
						let csvCur = extraitsUncat.find( ext => ext.year === c.year );
						csvCur.extraits += c.phrases2.length;
						csvCur.phrases = csvCur.phrases.concat(c.phrases2); 
					}
					else{
						extraitsUncat.push(
							{
								year:c.year,
								extraits: c.phrases2.length,
								phrases: c.phrases2
							}
						)
					}
				}
			)
		}
		// console.log('extraitsUNCAT',extraitsUncat);
		// console.log('csv monde filtre',filtre,csvMonde);
		const csvDataNonMonde = [];
		const csvNonMonde = [];
		const fluxValid = [];
		for (const i in this.user.flux) {
			let ok = false;
			if (this.user.flux[i].isMonde) {
				continue;
			}
			if (this.user.flux[i].selected) {
				ok = true;
			}
			if (!this.checkFluxFilter()) {
				ok = true;
			}
			if (ok) {
				if (this.user.flux[i].selected && !this.user.flux[i].isMonde && this.user.flux[i].flux.toLowerCase() !== "monde") {
					fluxValid.push(this.user.flux[i].flux);
				}
				const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
					let res = false;
					if (this.cleanString(a.flux) === this.cleanString(this.user.flux[i].flux)) {
						res = true;
					}
					return res;
				});
				for(const idEnj in enjeuxArray){
					for (const c in csvPhrases) {
						if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
							if(filtre === 1){
								if (csvDataNonMonde.find((csv) => csv.nomCsv === csvPhrases[c].nom && csv.enjeu === enjeuxArray[idEnj].key )) {
									continue;
								}
								let phrasesActif = [];
								let phrasesUncategorized = [];
								this.phrases[csvPhrases[c].nom].map(
									(value) => {
										// if ("#" + enjeuxArray[idEnj].num === value.enjeu){
										if ( enjeuxArray[idEnj].num === value.enjeu){
											phrasesActif.push(value);
											if(value.enjeu_2_color !== 'uncategorized' ){
											}
											else{
												phrasesUncategorized.push(value);
											}
										}
									}
								)
								if(phrasesActif.length > 0 || phrasesUncategorized.length > 0){
									csvDataNonMonde.push({
										nomCsv: csvPhrases[c].nom,
										enjeu: enjeuxArray[idEnj].key
									});
									let dateCsv = new Date(csvPhrases[c].date);
									let anneeCsv = ''+dateCsv.getFullYear(); 
									csvNonMonde.push(
										{ 
											enjeu: enjeuxArray[idEnj].key,
											year:anneeCsv ,
											// year:csvPhrases[c].date.split('/')[2] ,
											csv:csvPhrases[c],
											csvNom: csvPhrases[c].nom,
											flux: csvPhrases[c].flux,
											phrases:phrasesActif,
											phrases2: phrasesUncategorized
										}
									)
								}
							}
							if(filtre === 2){
								if(csvPhrases[c].enjeu_2_color && Object.keys(csvPhrases[c].enjeu_2_color).length !== 0){
									for(const enjeu in csvPhrases[c].enjeu_2_color){
										if (this.selectedEnjeux.find((enj) => enj.num === enjeu)) {
											if(csvPhrases[c].enjeu_2_color[enjeu] ===  enjeuxArray[idEnj].num){
												if (csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom && csv.enjeu === enjeuxArray[idEnj].key && csv.enjeuOther === enjeu)) {
													continue;
												}
												let phrasesActif = [];
												this.phrases[csvPhrases[c].nom].map(
													(value) => {
														if ("#" + enjeu === value.enjeu){
															if(value.enjeu_2_color !== 'uncategorized' ){
																phrasesActif.push(value);
															}
														}
													}
												)
												if(phrasesActif.length > 0){
													csvDataNonMonde.push({nomCsv: csvPhrases[c].nom , enjeu:enjeuxArray[idEnj].key, enjeuOther:enjeu});
													let dateCsv = new Date(csvPhrases[c].date);
													let anneeCsv = ''+dateCsv.getFullYear(); 
													csvNonMonde.push(
														{
															enjeu: enjeuxArray[idEnj].key,
															enjeuOther: enjeu,
															// year: csvPhrases[c].date.split('/')[2], 
															year: anneeCsv, 
															csv: csvPhrases[c],
															csvNom: csvPhrases[c].nom,
															flux: csvPhrases[c].flux,
															phrases: phrasesActif
														}
													)
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		// console.log('csvNonMonde',filtre,csvNonMonde);
		// console.log('fluxValid',fluxValid);
		if(filtre === 1){
			let csvCopie = csvNonMonde.slice();
			let tempTab = [];
			let tempTab2 = [];
			for(let index = fluxValid.length - 1;index >= 0; index-- ){
				tempTab2.push(fluxValid[index]);
			}
			csvCopie.map(
				c => {
					// if(!tempTab.find( d => d.year  === c.year && d.flux === c.flux && d.nom === c.csvNom )){
					if(!tempTab.find( d => d.year  === c.year && d.flux === c.flux  )){
						let idFlux = 0;
						tempTab2.forEach(
							(f,i) => {
								if(this.cleanString(f) === this.cleanString(c.flux) ) idFlux = i;
							}
						)

						tempTab.push(
							{
								year: c.year,
								flux: c.flux,
								idflux: idFlux,
								nom: c.csvNom,
								phrasesUncat: c.phrases2
							}
						)
					}
					else{
						let dataCur = tempTab.find( d => d.year  === c.year && d.flux === c.flux );
						dataCur.phrasesUncat = dataCur.phrasesUncat.concat(c.phrases2);
					}
				}
			)
			extraitsUncat2 = tempTab;
			// console.log(extraitsUncat2);
		}
		// console.log('les csv non monde filitre ',filtre,csvNonMonde);
		for (const enjeu of enjeuxArray) {
			if(filtre === 1) {
				if (enjeu.enjeu_big !== this.categorySelected) {
					continue;
				}
			}
			
			// console.log("enjeu", enjeu);
			this.years.forEach((year) => {
				const ind = enjeu.key;
				const data = [];

				let legend = "";

				if (this.hideEnjeu) {
					// console.log("hide enjeu actif");
					legend = this.motCle;
				} else {
					legend = this.translate.currentLang == 'fr' ? this.fr(filtre === 1 ? enjeu.pre_def : enjeu.titre).split(".")[0] : this.en(filtre === 1 ? enjeu.pre_def : enjeu.titre).split(".")[0];
				}
				if (!legendData.includes(legend)) {
					legendData.push(legend);
				}
			


				if(this.mondeSelected){
					csvMonde.map(
						(csv) => {
							let curEnj = filtre === 1 ? "#"+csv.enjeu : csv.enjeu;
							
							if(csv.year === year && curEnj === enjeu.num){
								if(data.find((d) => d.year === year)){
									let datacur = data.find((d) => d.year === year);
									let threeFirstPhrases = [];
									datacur.value += csv.phrases.length;
									csv.phrases.forEach( p => {
										if(threeFirstPhrases.length < 3) threeFirstPhrases.push(p.phrase.substr(0,100)+"...");
									});
									datacur.phrases = datacur.phrases.concat(threeFirstPhrases);
									if(datacur.phrases.length >= 3){
										datacur.phrases.splice(3,datacur.phrases.length-3);
									}
								}
								else{
									let threeFirstPhrases = [];
									csv.phrases.forEach( p => {
										if(threeFirstPhrases.length < 3) threeFirstPhrases.push(p.phrase.substr(0,100)+"...");
									});
									data.push({ value: csv.phrases.length, unit: this.translate.currentLang == 'fr' ? " extrait" : " extract", year: year, csvNomx: csv.csvnom , info: "Monde", phrases:threeFirstPhrases});
								}		
							}
							
						}
					)
					
					
				}
				csvNonMonde.map(
					c => {
						let result = 0;
						let csvNom = "";
						let threeFirstPhrases = [];
						let info = "";
						let enjeuCur = (filtre === 1) ? "#"+c.enjeu : c.enjeu;
						if(c.year === year && enjeuCur === enjeu.num ){
							this.nbExtraits += c.phrases.length;
							if(c.phrases.length > 0){
								c.phrases.forEach( p => {
									if(threeFirstPhrases.length < 3) threeFirstPhrases.push(p.phrase.substr(0,100)+"...");
								});	
								
								result = c.phrases.length;
								csvNom = c.csvNom
								for(let index = fluxValid.length - 1;index >= 0; index-- ){
									if(this.cleanString(c.flux) === this.cleanString(fluxValid[index])){
										if(filtre === 1){
											if(data.find( d => d.year === year && d.enjeu === enjeuCur )){
												let dataCur = data.find( d => d.year === year && d.enjeu === enjeuCur );
												dataCur.value = dataCur.value + result; 
												dataCur.phrases = dataCur.phrases.concat(threeFirstPhrases); 
											}
											else{
												data.push(
													{ 
														value: result, 
														unit: this.translate.currentLang == 'fr' ? " extrait" : " extract", 
														year: year,
														csvNom:csvNom,
														enjeu:enjeuCur,
														flux: c.flux,
														info: info,
														phrases:threeFirstPhrases
													 }
												);
											}
										}
										else{
											data.push(
												{ 
													value: result, 
													unit: this.translate.currentLang == 'fr' ? " extrait" : " extract",
													year: year,
													csvNom:csvNom,
													enjeu:enjeuCur,
													flux: c.flux,
													info: info,
													phrases:threeFirstPhrases 
												}
											);
										}
									}
									else{
										// if(this.mondeSelected){
											data.push({ value: 0, unit: this.translate.currentLang == 'fr' ? " extrait" : " extract", year: '',csvNom:csvNom+'ajoutcoco',enjeu:'',info: info,phrases:threeFirstPhrases });
										// }
									}
								}
								if(this.mondeSelected){
									while(true){
										if(data.length < fluxValid.length + 1){
											data.splice(0,0 ,{ value: 0, unit: this.translate.currentLang == 'fr' ? " extrait" : " extract", year: year,csvNom:'ajoutcoco',info: info,phrases:threeFirstPhrases })
										}
										else{
											break;
										}
									}
								}
								// if(!this.mondeSelected){
								// 	fluxValid.forEach(
								// 		f => {
								// 			if(data.length < fluxValid.length){
								// 				data.splice(0,0 ,{ value: 0, unit: this.translate.currentLang == 'fr' ? " extrait" : " extract", year: year,csvNom:'ajoutcoco',info: info,phrases:threeFirstPhrases })
								// 			}
								// 		}
								// 	)
								// }
							}
						}	
					}
				)
				
				const serieChart = {
					name: legend,
					type: "bar",
					stack: "总量",
					itemStyle: {
						color: filtre === 1 ? colors[iteration % colors.length] : this.getColor(enjeu.num)
					},
					label: {
						normal: {
							show: true,
							position: "inside",
							formatter: function (data) {
								const v = data.value;
								if (v > 0) {
									return v;
								} else {
									return "";
								}
							}
						}
					},
					data: data,
				};
				chartOption.series.push(serieChart);



			});
			iteration++;
		}

		// console.log("list this.user.flux : ", this.user.flux);

		const i = 0;
		if (this.mondeSelected) {
			yAxisData.push("Monde");
		}
		for (let index = this.user.flux.length - 1; index >= 0; index--) {
			const emet = this.user.flux[index];
			if (emet.selected && !emet.isMonde && emet.flux.toLowerCase() !== "monde") {
				yAxisData.push(emet.flux);
			}
		}

		// console.log(" Y data : ",yAxisData);

		// for (let emet of this.user.flux) {
		// 	if (emet.selected) {
		// 		yAxisData.push(emet.flux);
		// 	}
		// 	i++;
		// }

		const objFlux = {};

		for (const flux of yAxisData) {
			const flux_formatted = flux.toLowerCase().trim().replace(/\s/g, "");
			objFlux[flux_formatted] = flux;
		}

		const self = this;

		chartOption.yAxis = {
			type: "category",

			axisLabel: {
				formatter: function (value) {
					if (value === "monde") {

						return "{" + value + "| }";
						return value[0].toUpperCase() + value.slice(1);
					} else {
						if (this.imageExists(this.allOrgaImageUrl[value])) {
							console.log("calalallalal", "{" + value + "| }");
							return "{" + value + "| }";
						} else {
							return objFlux[value];
						}
					}
				},
				margin: 10
			}
		};

		// console.log("this.allOrgaImageUrl : ", this.allOrgaImageUrl);

		const richValue = {};
		const formatted_yAxisData = [];

		for (const flux of yAxisData) {

			const flux_formatted = flux.toLowerCase().trim().replace(/\s/g, "");

			formatted_yAxisData.push(flux_formatted);
			// if(this.imageExists(this.allOrgaImageUrl[flux_formatted])){

			const imgOption = {
				// width: 110,
				height: 45,
				// fontSize: 40,
				align: "center",
				backgroundColor: {
					image: this.allOrgaImageUrl[flux_formatted] ? this.allOrgaImageUrl[flux_formatted] : ""
				}
			};

			richValue[flux_formatted] = imgOption;

			// }

		}

		// chartOption.yAxis.data = yAxisData;
		chartOption.yAxis.data = formatted_yAxisData;

		// Construct image orga
		// chartOption.yAxis.axisLabel.rich = {
		// 	value: {
		// 		lineHeight: 30,
		// 		align: "center"
		// 	},
		// 	...JSON.parse(JSON.stringify(richValue))
		// };
		// console.log('legendData',legendData);
		// console.log('legendData 2',legendData_2);
		chartOption.legend = { data: legendData, type: "scroll", show: true };
		// console.log("chartOption : ", chartOption);

		const date = Date.now();
		let enjeux: string = this.orga.organisation;
		let index = 0;

		for (const emet of this.user.flux) {
			if (emet.selected && emet.flux.trim().toLowerCase() !== this.orga.organisation.trim().toLowerCase() && !emet.isMonde && emet.flux.toLowerCase() !== "monde") {
				enjeux += ", " + emet.flux;
			}
			index++;
		}
		if (monde) {
			enjeux += ", Monde";
		}
		let title = "Classement des enjeux écologiques traités par " + enjeux;

		title += "<br>Sur " + this.csvPhrasesFiltered.length + " documents de références soit " + this.nbExtraits + " extrait" + (this.nbExtraits > 1 ? "s" : "") + "<br>Source : AmplifyNature " + (new Date()).getFullYear() + " Copyright";

		let titleen = "Ranking of ecological issues addressed by " + enjeux;

		titleen += "<br>Out of " + this.csvPhrasesFiltered.length + " reference documents, i.e "+ this.nbExtraits + " extract"+ (this.nbExtraits > 1 ? "s" : "") + "<br>Source : AmplifyNature " + (new Date()).getFullYear() + " Copyright";
		// let additionalChartTitle = this.checkMotCleExist != "" ? "Qui utilise le mot clé \"" + this.motCle + "\" ?" : "Qui s'engage sur ";
		let additionalChartTitle = this.hideEnjeu ? "Qui utilise le mot clé \"" + this.motCle + "\" ?" : "Qui s'engage sur ";
		let additionalCharTitleEn = this.hideEnjeu ? "Who uses the keyword \"" + this.motCle + "\" ? " : "Who is committed "
		if (!this.hideEnjeu) {
			switch (this.categorySelected) {
				case "eau":
					additionalChartTitle += `le zéro pollution de l'eau ?`;
					break;

				case "biodiversité":
					additionalChartTitle += `le zéro perte de biodiversité ?`;
					break;

				case "sol":
					additionalChartTitle += `le zéro artificialisation des sols ?`;
					break;

				case "co2":
					additionalChartTitle += `la neutralité carbone ?`;
					break;

				case "équité":
					additionalChartTitle += `le zéro pollution de l'air ?`;
					break;

				default:
					// code...
					additionalChartTitle += `le zéro pollution de l'eau ?`;
					break;
			}
		}

		if (!this.hideEnjeu) {
			switch (this.categorySelected) {
				case "eau":
					additionalCharTitleEn += `to zero water pollution ?`;
					break;

				case "biodiversité":
					additionalCharTitleEn += `to zero loss of biodiversity ?`;
					break;

				case "sol":
					additionalCharTitleEn += `to zero soil artificialization ?`;
					break;

				case "co2":
					additionalCharTitleEn += `to carbon neutrality ?`;
					break;

				case "équité":
					additionalCharTitleEn += `to zero air pollution ?`;
					break;

				default:
					// code...
					additionalCharTitleEn += `to zero water pollution ?`;
					break;
			}
		}

		const page = {
			// chartTitle: "Traitement des enjeux " + this.categorySelected.toUpperCase() + " par " + enjeux,
			langue: this.translate.currentLang,
			chartTitle: (this.translate.currentLang == 'fr') ? additionalChartTitle : additionalCharTitleEn,
			title: (this.translate.currentLang == 'fr') ? title : titleen,
			data: JSON.stringify(chartOption),
			type: 2,
			organisation: this.orga,
			date: date,
			enjeu_big: this.categorySelected,
			client: this.user,
			periode: this.year_from + "-" + this.year_to,
			orgaImageUrl: JSON.stringify(this.allOrgaImageUrl),
			flux: JSON.stringify(objFlux),
			extraits_uncategorized: JSON.stringify(extraitsUncat),
			extraits_uncategorized2: JSON.stringify(extraitsUncat2),
			meta: {
				title: additionalChartTitle,
				description: "",
				imageUrl: this.user.orga_img_url && this.user.orga_img_url !== "" ? this.user.orga_img_url : null,
				url: "https://amplifynature.com/graphique/" + this.orga.key + "/" + (filtre === 1 ? "2" : "22") + "/" + date
			}
		};
		return page;
	}

	getExtraits(emetteur: string, year: number, enjeu: Enjeu, filtre = 1) {
		let res = 0;
		let info = "";
		this.csvPhrasesFiltered.forEach((csvPhrase, index, array) => {

			let ok = false;
			if (year && this.cleanString(csvPhrase.flux) === this.cleanString(emetteur) && parseInt(csvPhrase.date.split("/")[2]) === year) {
				ok = true;
			} else if (!year && this.cleanString(csvPhrase.flux) === this.cleanString(emetteur)) {
				ok = true;
			}
			if (filtre === 1) {
				if (ok && csvPhrase.enjeu_2 && csvPhrase.enjeu_2[enjeu.num]) {
					for (const i in csvPhrase.enjeu_2[enjeu.num]) {
						if (Number.isInteger(csvPhrase.enjeu_2[enjeu.num][i])) {
							// if(emetteur.includes("Solvay")) {
							// 	console.log("EXTRAIT FOUND", emetteur, enjeu);
							// 	console.log( csvPhrase);
							// 	console.log( csvPhrase.enjeu_2[enjeu.num][i]);
							// }

							res += csvPhrase.enjeu_2[enjeu.num][i];
							info += this.lang(i + "_titre") + " : " + csvPhrase.enjeu_2[enjeu.num][i] + "<br/>";
						}

					}
				}
			} else if (filtre === 2) {
				if (ok && csvPhrase.enjeu_2) {
					for (const enj1 in csvPhrase.enjeu_2) {
						let extrait = 0;
						if (this.selectedEnjeux.find((enj) => enj.num === enj1)) {
							for (const enj2 in csvPhrase.enjeu_2[enj1]) {
								if (enj2 === enjeu.num && Number.isInteger(csvPhrase.enjeu_2[enj1][enj2])) {
									res += csvPhrase.enjeu_2[enj1][enj2];
									extrait += csvPhrase.enjeu_2[enj1][enj2];
								}
							}
						}

						// info += this.lang("##"+(parseInt(enj1) < 10 ? "0" : "")+enj1).split(".")[0] + " : " + extrait + "<br/>";
					}
				}
			}

		});
		return { extraits: res, info: info };
	}

	deconnexion() {
		localStorage.removeItem("isLoggedin");
		localStorage.removeItem("user");
		localStorage.removeItem("orga");
		this.router.navigate(["/login"]);
	}

	checkYear() {
		// this.options.barDimension = 100;
		const years = this.years.map((function (item) {
			return parseInt(item, 10);
		}));
		// if(this.dateFilterToggle == false || this.dateFilterToggle == undefined)
		if (this.dateFilterToggle === true) {

			const sliderArray = [];
			for (let i = 0; i < this.years.length; i++) {
				sliderArray.push({ value: this.years[i] });
			}

			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				if (this.chartOptions.series[1].data2.length == 0) {
					return this.couleur_annee_vide ;
				} else { return this.couleur_annee_existant ; }
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			// this.options.showTicks = false;
			this.options.stepsArray = sliderArray;

			this.value = Math.min(...years);


			this.yearFilter = this.value.toString();
			this.checkFilter();
			this.dateFilterToggle = false;
		}

	}

	checkTimelapse() {
		// this.options.barDimension = 100;
		this.yearFilter = false;
		const years = this.years.map((function (item) {
			return parseInt(item, 10);
		}));
		if (this.dateFilterToggle === false || this.dateFilterToggle === undefined) {
			const sliderArray = [];
			for (let i = 0; i < this.years.length; i++) {
				sliderArray.push({ value: this.years[i], legend: "" });
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			this.options.stepsArray = sliderArray;
			this.options.floor = Math.min(...years);
			this.options.ceil = Math.max(...years);
			this.value = Math.min(...years);
			this.highValue = Math.max(...years);


			this.year_from = this.value.toString();
			this.year_to = this.highValue.toString();
			this.test2 = false;
			this.dateFilterToggle = true;
			this.checkFilter();
		}
	}

	dateRangeChange(changeContext: ChangeContext) {
		this.year_from = changeContext.value.toString();
		this.year_to = changeContext.highValue.toString();
		this.loadingMap = true;
		this.checkFilter();
		this.selectTab('chart2');
		// console.log(document.querySelectorAll('.ng5-slider-selected'));
	}
	colorSlider() {
		let styleText = "<div id='style'><style>";
		if (this.year_from && this.year_to) {
			const yearCsvData = this.data_slider.map(
				(csvdata) => {
					return csvdata.year;
				}
			);
			const yearsIntervalle = this.years.filter(
				(value, index) => {
					return value >= this.year_from && value <= this.year_to;
				}
			);
			// let nbpoints = document.querySelectorAll('.ng5-slider-selected');
			// console.log(nbpoints);


			if (yearCsvData.includes(yearsIntervalle[0])) {
				styleText += " .ng5-slider-pointer-min{ background: " + this.couleur_annee_existant + " !important} ";
			} else {
				styleText += " .ng5-slider-pointer-min{ background: " + this.couleur_annee_vide + " !important} ";
			}
			if (yearCsvData.includes(yearsIntervalle[yearsIntervalle.length - 1])) {
				styleText += " .ng5-slider-pointer-max{ background: " + this.couleur_annee_existant + " !important} ";
			} else {
				styleText += " .ng5-slider-pointer-max{ background: " + this.couleur_annee_vide + " !important} ";
			}
			this.years.forEach(
				(year, index) => {
					const b = index + 1;
					if (yearCsvData.includes(year)) {
						styleText += ".ng5-slider-tick.ng5-slider-selected:nth-child(" + b + ") { background: " + this.couleur_annee_existant + " !important } ";
					} else {
						styleText += ".ng5-slider-tick.ng5-slider-selected:nth-child(" + b + ") { background: " + this.couleur_annee_vide + " !important } ";
					}
				}
			);
			styleText += '.ng5-slider-tick {background: '+ this.couleur_annee_defaut +' !important}';
			styleText += '.ng5-slider-full-bar .ng5-slider-span.ng5-slider-bar {background: '+ this.couleur_annee_defaut +' !important}';

		}
		styleText += "</style></div>";
		jQuery("#style").replaceWith(styleText);
	}
	dateSlideChange(changeContext: ChangeContext) {
		console.log('ici c dateSlidecHANGE')
		this.year_from = null;
		this.year_to = null;
		this.yearFilter = changeContext.value.toString();
		this.loadingMap = true;
		this.checkFilter();
	}

	ngAfterViewInit() {

		// sidebar
		this.isActive = false;
		this.isActive2 = true;
		this.isActive3 = false;
		this.collapsed2 = true;
		this.collapsed3 = true;
		this.collapsed = false;
		this.showMenu = "";
		this.pushRightClass = "push-right";
		this.pushLeftClass = "push-right";
		// this.defaultEnjeu();
	}
	onFluxFilterChange(e: any, flux: Flux, index) {
		if (flux.flux === "Monde") {
			this.mondeSelected = e;
			if (e) {
				for (const f in this.user.flux) {
					if (this.user.flux[f].isMonde) {
						this.user.flux[f].selected = e;
					}
				}
			}
		}

		// console.log("checkMotcle", this.checkMotcle)


		for (const f in this.user.flux) {
			if (this.user.flux[f].flux === flux.flux) {
				this.user.flux[f].selected = e;
			}
		}


		this.edit = true;
		let res = false;
		for (const f of this.user.flux) {
			if (!f.selected) {
				res = true;
				break;
			}
		}
		if (res) {
			this.allFluxFilter = false;
		} else {
			this.allFluxFilter = true;
		}

		this.checkFilter();

		(this.mapToogle === true && this.hide_graph === true && this.hide_map === false) && this.selectTab("chart2");

		this.sortEnjeux();

	}

	onNewPaysFilterChange(e: any, p) {
		const self = this;
		self.loadingMap = true;
		self.csvPhrasesFiltered = [];
		const test = true;
		// let res = false;
		// for(let i in this.newpaysFilter){
		// 	if(this.newpaysFilter[i]){
		// 		res = true;
		// 		this.allPaysFilter = false;
		// 		break;
		// 	}
		// }
		if (e) {
			self.allPaysFilter = false;
			// this.all_pays = "";
			this.all_pays = "";
			this.liste_pays.push(p);
		} else {
			for (let i = 0; i < this.liste_pays.length; i++) {
				if (this.liste_pays[i] === p) {
					this.liste_pays.splice(i, 1);
				}
			}
		}
		self.checkFilter();

	}

	onPerfFilterChange(e: any) {
		this.loadingMap = true;
		this.csvPhrasesFiltered = [];
		const self = this;
		self.allPerf = false;
		this.checkFilter();


	}

	onAllPaysFilterChange(e: any) {
		this.loadingMap = true;
		if (this.allPaysFilter) {
			this.all_pays = "Indifférent";
			this.liste_pays = [];
			for (const i in this.newpaysFilter) {
				this.newpaysFilter[i] = false;
			}
			this.allPaysFilter = false;
		} else {
			this.all_pays = "";
		}

		this.checkFilter("allPays");
	}


	onFluxAllFilterChange(e: any) {
		this.edit = true;
		const self = this;
		this.loadingMap = true;
		if (this.allFluxFilter) {

			for (const i in this.user.flux) {
				this.user.flux[i].selected = true;
			}
			// this.allFluxFilter = false;
			self.years = [];
			self.csvPhrases.map((a: CsvPhrase) => {
				if (a.date.split("/").length === 3) {
					if (parseInt(a.date.split("/")[2]) >= 2000) {
						self.years.push(a.date.split("/")[2]);
					}
				}

				return a;
			});
			this.years = this.years.filter((v, i) => this.years.indexOf(v) === i);
			this.years = this.years.sort(function (a, b) { return parseInt(a) - parseInt(b); });
			this.filter = false;
			this.mondeSelected = true;
			this.checkFilter();
		}

		if (e === false) {
			for (const i in this.user.flux) {
				this.user.flux[i].selected = false;
			}
			this.loadingMap = false;
			this.mondeSelected = false;
			this.checkFilter();


		}
		(this.mapToogle === true && this.hide_graph === true && this.hide_map === false) && this.selectTab("chart2");
		this.dateFilterToggle = false;
		this.checkTimelapse();




		this.showSlider = false;
		setTimeout(() => {
			this.showSlider = true;
		}, 50);

	}

	resetFluxFilter(e: any) {

		const self = this;
		// this.loadingMap = true;
		if (this.allFluxFilter) {
			// this.allFluxFilter = false;
			self.years = [];
			self.csvPhrases.map((a: CsvPhrase) => {
				if (a.date.split("/").length === 3) {
					if (parseInt(a.date.split("/")[2]) >= 2000) {
						self.years.push(a.date.split("/")[2]);
					}
				}

				return a;
			});
			// this.years =  this.years.filter((v,i) => this.years.indexOf(v) === i);
			this.years = this.years.filter((v, i) => this.years.indexOf(v) === i && v !== undefined && parseInt(v) > 2000);
			this.years = this.years.sort(function (a, b) { return parseInt(a) - parseInt(b); });
			// this.checkFilter("allFlux");
		}
		this.test2 = false;
		this.yearFilter = false;
		this.year_from = undefined;
		this.year_to = undefined;

		this.showSlider = false;
		setTimeout(() => {
			this.showSlider = true;
		}, 50);
	}

	onAllPerfFilterChange(e: any) {
		const self = this;
		this.loadingMap = true;

		// if(self.allPerf){
		for (const i in self.perfFilter) {
			self.perfFilter[i] = false;
		}
		self.allPerf = false;
		// }



		this.checkFilter("allPerf");
	}

	onYearFilterChange(e: any) {
		// this.options.barDimension = 100;
		this.showFilterDate = this.yearFilter === "definie" ? true : false;
		this.showSliderDate = this.yearFilter === "slider" ? true : false;
		this.showRangeDate = this.yearFilter === "range" ? true : false;
		this.loadingMap = true;
		if (this.yearFilter === "slider") {
			const sliderArray = [];
			for (let i = 0; i < this.years.length; i++) {
				sliderArray.push({ value: this.years[i], legend: "" });
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			this.options.stepsArray = sliderArray;
			this.value = 2014;
		}
		if (this.yearFilter === "range") {
			const sliderArray = [];
			for (let i = 0; i < this.years.length; i++) {
				sliderArray.push({ value: this.years[i], legend: "" });
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			this.options.stepsArray = sliderArray;
			this.value = 2014;
			this.highValue = 2019;
		}

		// this.checkFilter();

	}

	onDateFilterChange(e: any, type: string) {
		switch (type) {
			case "month_from":
				if (this.year_from) {
					this.loadingMap = true;
					this.checkFilter();
				}
				break;
			case "year_from":
				if (this.month_from) {
					this.loadingMap = true;
					this.checkFilter();
				}
				break;
			case "month_to":
				if (this.year_to) {
					this.loadingMap = true;
					this.checkFilter();
				}
				break;

			case "year_to":
				if (this.month_to) {
					this.loadingMap = true;
					this.checkFilter();
				}
				break;

			default:
				// code...
				break;
		}
	}

	onPaysFilterChange(e) {

		this.loadingMap = true;
		this.checkFilter();

	}

	getChartOptions2() {
		this.fluxData = [];
		const csvData = [];
		let sumCsvData = [];
		const self = this;
		// console.log('getChartOptions2',this.selectedEnjeux);
		// console.log('mots cle ',this.selectedMotsCle);
		console.log(' dans getChartOptions2 csvPhrasesFiltered ',this.csvPhrasesFiltered);
		const enjeuxActifs = (!self.checkMotcle) ? self.selectedEnjeux : self.allEnjeux;
		// get flux
		if (!this.checkMotcle && self.selectedEnjeux.length === 0) {
			if (self.client_enjeu && self.client_enjeu.length > 0) {
				self.selectedEnjeux = [];
				for (const enj of self.client_enjeu) {
					if (enj.enjeu_big === self.categorySelected) {
						self.selectedEnjeux.push(self.client_enjeu[0]);
						break;
					}
				}

			} else {
				return;
			}
		}

		// Non Monde
		for (const i in this.user.flux) {
			let ok = false;
			const isMonde = false;
			if (this.user.flux[i].isMonde) {
				continue;
			}
			// console.log("this.user.flux[i] %o, SELECTED %o", this.user.flux[i], this.user.flux[i].selected);
			if (this.user.flux[i].selected) {
				ok = true;
			}
			if (!this.checkFluxFilter()) {
				ok = true;
			}


			if (ok) {
				// console.log("csvPhrasesFiltered", this.csvPhrasesFiltered);
				const csvPhrases = this.csvPhrasesFiltered.filter(function (a) {
					let res = false;
					if (self.cleanString(a.flux) === self.cleanString(self.user.flux[i].flux)) {
						res = true;
					}
					return res;
				});
				// console.log("csvPhrasesFiltered filtered, flux", this.csvPhrasesFiltered.filter((c) => c.nom == "CSV3_0910"), self.user.flux[i].flux);
				const promises = [];

				let totalFlux = 0;
				for(const idEnj in enjeuxActifs){
					for (const c in csvPhrases) {
						let total = 0;



						if (self.cleanString(csvPhrases[c].flux) === self.cleanString(self.user.flux[i].flux)) {
							// console.log("csv %o, flux %o",csvPhrases[c], csvPhrases[c].flux);
							total = total + 1;

							let itemStyle: any = {};
							if (enjeuxActifs[idEnj] && csvPhrases[c].enjeu_2_color && csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idEnj].key]) {
								itemStyle = { color: this.getColor(csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idEnj].key]) };

								if (csvPhrases[c].nom === self.csvSelected) {
									// itemStyle = { color: this.couleurPhrase[sat], borderWidth: 1, borderColor: '#004085' };
									itemStyle = { color: this.getColor(csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idEnj].key]), borderWidth: 1, borderColor: "#004085" };
								}
							} else {
								itemStyle = { color: this.getColor("Objectif non traité") };
								// itemStyle = { color: "#ff0000" };
								if (csvPhrases[c].nom === self.csvSelected) {
									// itemStyle = { color: this.couleurPhrase[sat], borderWidth: 1, borderColor: '#004085' };
									itemStyle = { color: this.getColor("Objectif non traité"), borderWidth: 1, borderColor: "#004085" };
								}
							}
							/**
							 * la couleur dans graphe doit être de la même couleur que la première phrase coloré du csv en question
							 * algo: appelé directement les phrases correpondant au csv en fonction de l'enjeu, puis checker la première phrase coloré (phrase dont enjeu_2_color !== uncategorized)
							 * les lignes de code qui suit écrase la valeur de itemStyle
							 */

							
							if(this.phrases[csvPhrases[c].nom]){
								if(this.phrases[csvPhrases[c].nom].length){
									if(this.checkMotcle){
										itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("uncategorized"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("uncategorized") };
									}
									else{
										this.phrases[csvPhrases[c].nom].some( (value: Phrase) => {
											// if ("#" + enjeuxActifs[idEnj].num === value.enjeu) {
											if (enjeuxActifs[idEnj].num === value.enjeu) {

												if(value.enjeu_2_color !== 'uncategorized'){
													itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor(value.enjeu_2_color), borderWidth: 1,borderColor: "#004085" } :{ color: this.getColor(value.enjeu_2_color) };	
													return true
												}
												else itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("uncategorized"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("uncategorized") };
											}
										});
									}
								}
								else itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("Objectif non traité"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("Objectif non traité") };
							}
							
							if (csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom)) {
								let csvDataCur = csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom);
								if(csvDataCur.itemStyle.color === this.getColor("Objectif non traité") && itemStyle.color !== this.getColor("Objectif non traité"))
								csvDataCur.itemStyle = itemStyle;
								
								continue;
							}
							csvData.push({
								value: total,
								name: "extraits",
								itemStyle: itemStyle,
								nomCsv: csvPhrases[c].nom,
								flux: csvPhrases[c].flux,
								year: csvPhrases[c].date.split("/")[2],
								titre: self.translate.currentLang == "en" ? csvPhrases[c].titre_en.substring(0, 20) : csvPhrases[c].titre_fr.substring(0, 20)

							});



						}
						totalFlux = totalFlux + 1;


					}

				}
				self.fluxData.push({
					value: totalFlux,
					name: self.user.flux[i].isMonde ? "Monde" : self.user.flux[i].flux,
					itemStyle: { color: self.getColor(self.user.flux[i].flux) },
					documents: csvPhrases.length
				});


			}



		}
		
		// Monde
		let isMonde = false;
		let totalFlux = 0;
		let documents = 0;
		let correction = 0;
		// count monde docs
		for (const i in this.user.flux) {
			let ok = false;
			if (this.user.flux[i].isMonde && this.mondeSelected) {
				ok = true;
				isMonde = true;
			}

			if (ok) {
				const csvPhrases = this.csvPhrasesFiltered.filter(function (a) {
					let res = false;
					if (self.slug(a.flux) === self.slug(self.user.flux[i].flux)) {
						res = true;
					}
					return res;
				});

				// console.
				for(const idenj in enjeuxActifs){
					for (const c in csvPhrases) {
						const total = 0;

						if (self.slug(csvPhrases[c].flux) === self.slug(self.user.flux[i].flux)) {
							if (enjeuxActifs[idenj] && (!csvPhrases[c].enjeu_2_color || !csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idenj].key])) {
								if(!self.checkMotcle) continue;
							}
							documents += csvPhrases.length;
							totalFlux = totalFlux + 1;
							// console.log("csv phrases", csvPhrases[c]);
						}



					}
				}
			}
		}
		// console.log("self.selectedEnjeux[0]", self.selectedEnjeux[0]);
		// console.log("totalFlux %o", totalFlux);
		if (totalFlux < this.maxMonde) {

			const diff = this.maxMonde - totalFlux;
			correction = diff / totalFlux;
			// console.log("correction %o", correction);

		}

		totalFlux = 0;
		documents = 0;
		for (const i in this.user.flux) {
			let ok = false;

			// console.log("this.user.flux[i] %o, SELECTED %o", this.user.flux[i], this.user.flux[i].selected);
			if (this.user.flux[i].isMonde && this.mondeSelected) {
				// console.log('les flux vaide',this.user.flux[i]);
				ok = true;
				isMonde = true;
			}

			if (ok) {
				const csvPhrases = this.csvPhrasesFiltered.filter(function (a) {
					let res = false;
					if (self.cleanString(a.flux) === self.cleanString(self.user.flux[i].flux)) {
						res = true;
					}
					return res;
				});

				// console.log("csvPhrasesFiltered filtered monde, flux", csvPhrases);
				for(const idenj in enjeuxActifs) {
					for (const c in csvPhrases) {
						let total = 0;
						if (self.cleanString(csvPhrases[c].flux) === self.cleanString(self.user.flux[i].flux)) {
							// console.log("csv %o, flux %o",csvPhrases[c], csvPhrases[c].flux);
							total = total + 1;

							let itemStyle: any = {};


							if (enjeuxActifs[idenj] && csvPhrases[c].enjeu_2_color && csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idenj].key]) {
								itemStyle = { color: this.getColor(csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idenj].key]) };

								if (csvPhrases[c].nom === self.csvSelected) {
									// itemStyle = { color: this.couleurPhrase[sat], borderWidth: 1, borderColor: '#004085' };
									itemStyle = { color: this.getColor(csvPhrases[c].enjeu_2_color["#" + enjeuxActifs[idenj].key]), borderWidth: 1, borderColor: "#004085" };
								}
							} else if (!self.checkMotcle) {
								continue;
							} else {
								itemStyle = { color: this.getColor("Objectif non traité") };
								if (csvPhrases[c].nom === self.csvSelected) {
									itemStyle = { color: this.getColor("Objectif non traité"), borderWidth: 1, borderColor: "#004085" };
								}
							}
							/**
							 * la couleur dans graphe doit être de la même couleur que la première phrase coloré du csv en question
							 * algo: appelé directement les phrases correpondant au csv en fonction de l'enjeu, puis checker la première phrase coloré (phrase dont enjeu_2_color !== uncategorized)
							 * les lignes de code qui suit écrase la valeur de itemStyle si this.phrases[csvPhrases[c].nom !== undefined
							 */
							 if(this.phrases[csvPhrases[c].nom]){
								if(this.phrases[csvPhrases[c].nom].length){
									if(this.checkMotcle){
										itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("uncategorized"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("uncategorized") };
									}
									else{
										this.phrases[csvPhrases[c].nom].some( (value: Phrase) => {
											// if ("#" + enjeuxActifs[idenj].num === value.enjeu) {
											if ( enjeuxActifs[idenj].num === value.enjeu) {
												if(value.enjeu_2_color !== 'uncategorized'){
													itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor(value.enjeu_2_color), borderWidth: 1,borderColor: "#004085" } :{ color: this.getColor(value.enjeu_2_color) };	
													return true
												}
												else itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("uncategorized"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("uncategorized") };
											}
										});
									}
								}
								else itemStyle =  (csvPhrases[c].nom === this.csvSelected) ? { color: this.getColor("Objectif non traité"), borderWidth: 1, borderColor: "#004085" } : { color: this.getColor("Objectif non traité") };
							}
							// console.log("flux ok 1");
							if (csvData.find((csv) => csv.nomCsv === csvPhrases[c].nom)) {
								continue;
							}
							csvData.push({
								value: total + correction,
								name: "extraits",
								itemStyle: itemStyle,
								nomCsv: csvPhrases[c].nom,
								flux: csvPhrases[c].flux,
								year: csvPhrases[c].date.split("/")[2],
								titre: self.translate.currentLang === "en" ? csvPhrases[c].titre_en.substring(0, 20) : csvPhrases[c].titre_fr.substring(0, 20)

							});
							documents += csvPhrases.length;
							totalFlux = totalFlux + 1;
						}



					}
				}


			}
		}
		sumCsvData = csvData.slice();
		this.data_slider = sumCsvData;
		this.colorSlider();

		// console.log("totalFlux Monde", totalFlux);

		if (totalFlux === 0 && this.mondeSelected) {
			
				csvData.push({
					value: this.maxMonde,
					name: "extraits",
					itemStyle: { color: this.getColor("Objectif non traité") },
					nomCsv: "",
					flux: "",
					year: "",
					titre: "Pas de document Monde"
	
				});
			
		}

		if (isMonde && this.mondeSelected) {
			self.fluxData.push({
				value: this.maxMonde,
				name: "Monde",
				itemStyle: { color: self.getColor("monde") },
				documents: totalFlux
			});
		}
	
		const options = {

			tooltip: {
				trigger: "item",
				formatter: function (params) {
					// console.log("params: %o",params);
					let data = "";
					if (params.seriesIndex === 0) {
						data = params.marker + "" + params.name.toUpperCase() + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" + params.data.documents + " documents";
					} else {
						data = params.marker + " " + params.data.titre + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" + params.data.flux;
					}

					return data;
				}

			},
			// legend: {
			//     orient: 'vertical',
			//     x: 'left',
			//     data:['直达','营销广告','搜索引擎','邮件营销','联盟广告','视频广告','百度','谷歌','必应','其他']
			// },
			series: [
				{
					name: "",
					type: "pie",
					selectedMode: "single",
					radius: [0, "30%"],
					label: {
						normal: {
							position: "inner",
							show: (this.showEtiquetteGrap) ? false : true,
							formatter: function(params){
								// console.log('parmas cercle interieur',params);
								if(params.percent > 7){
									return params.name;
								}
								else return '';	
							}
						}
					},
					labelLine: {
						normal: {
							show: false
						}
					},
					emphasis: {
						focus: 'self',
						label:{
							show:false
						}
					},
					data: this.fluxData.map(
						data => {
							if(this.hideEnjeu){
								if(data.name == 'Monde' && data.value > 0){
									data.value = data.documents;
								}
							}
							return data;
						}
					)
				} ,
				{
					/*name:'Document',*/
					name: "",
					type: "pie",
					radius: ["40%", "55%"],
					label: {
						normal: {
							formatter: function(params){
								// console.log('parmas cercle exterieur',params);
								if(params.percent > 7){
									return '';
								}
								else return '';	
							},
							// formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c} ',
							backgroundColor: "#eee",
							borderColor: "#aaa",
							borderWidth: 1,
							borderRadius: 4,
							show: false,
							// shadowBlur:3,
							// shadowOffsetX: 2,
							// shadowOffsetY: 2,
							// shadowColor: '#999',
							// padding: [0, 7],
							rich: {
								a: {
									color: "#999",
									lineHeight: 22,
									align: "center"
								},
								// abg: {
								//     backgroundColor: '#333',
								//     width: '100%',
								//     align: 'right',
								//     height: 22,
								//     borderRadius: [4, 4, 0, 0]
								// },
								hr: {
									borderColor: "#aaa",
									width: "100%",
									borderWidth: 0.5,
									height: 0
								},
								b: {
									fontSize: 16,
									lineHeight: 33
								},
								per: {
									color: "#eee",
									backgroundColor: "#334455",
									padding: [2, 4],
									borderRadius: 2
								}
							}
						}
					},
					data: csvData,
					data2: sumCsvData
				}
			]
		};
		return options;
	}



	onChartEvent(e: any, type: string) {
		this.csvClick = true;
		this.sideEnjeux = false;
		console.log("onChartEvent");
		let index = 0;
		if (e.data.nomCsv === "") {
			this.csvClick = false;
			this.collapsed2 = true;
			this.collapsedEvent.emit(this.collapsed2);
		}
		if (e.seriesIndex === 1 && e.data.nomCsv !== "") {
			this.csvSelected = e.data.nomCsv;
			for (const i in this.csvPhrasesFiltered) {
				if (e.data.nomCsv === this.csvPhrasesFiltered[i].nom) {
					this.csvPhrase = this.csvPhrasesFiltered[i];
					break;
				}
				index += 1;
			}
			const self = this;
			// console.log("selected enjeux : %o", self.selectedEnjeux);
			if (self.phrases[this.csvPhrase.nom] === undefined) {
				console.log("self.phrases[this.csvPhrase.nom] === undefined"); 
				self.phrasesLoading = true;
				self.documentService.listPhrasesByDocWithSeuil(this.csvPhrase.id).subscribe((phrasesData) => {
					const phrases: Phrase[] = [];
					phrasesData.forEach((child) => {
						const phrase: Phrase = new Phrase().deserialize(child);
						phrases.push(phrase);
						return false;
					});

					self.phrases[self.csvPhrase.nom] = phrases;

					self.csvPhrase.phrases = self.phrases[this.csvPhrase.nom].filter(function (value: Phrase, index: number, array: Phrase[]) {
						let ok = false;
						if (self.selectedEnjeux.length > 0) {
							for( const i in self.selectedEnjeux) {
								// if ("#" + self.selectedEnjeux[i].num === value.enjeu) {
								if (self.selectedEnjeux[i].num === value.enjeu) {
									ok = true;
								}
							}
						} else if (self.checkMotcle) {
							ok = true;
						}
						return ok;
					});
					self.phrasesLoading = false;
					console.log("phrases from listPhrasesByDocWithSeuil: %o", self.csvPhrase.phrases);
				});
			} else {
				self.csvPhrase.phrases = self.phrases[this.csvPhrase.nom].filter(function (value: Phrase, index: number, array: Phrase[]) {
					let ok = false;
					if (self.selectedEnjeux.length > 0) {
						for( const i in self.selectedEnjeux) {
							if ( self.selectedEnjeux[i].num === value.enjeu) {
							// if ("#" + self.selectedEnjeux[i].num === value.enjeu) {
								ok = true;
							}
						}
					} else if (self.checkMotcle) {
						ok = true;
					}
					return ok;
				});
				console.log("phrases : %o", self.csvPhrase.phrases);
				self.phrasesLoading = false;
			}
			self.collapsed2 = false;
			self.collapsedEvent.emit(self.collapsed2);


		}
		if(this.csvPhrase.categorie != 8){
			let titre_en = (this.csvPhrase.titre_en) ? this.csvPhrase.titre_en.split('_') : [];
			let titre_fr = (this.csvPhrase.titre_fr) ? this.csvPhrase.titre_fr.split('_') : [];
			titre_en.splice(0,1); 
			titre_fr.splice(0,1);
			this.csvPhrase.titre_en = titre_en.join(' '); 
			this.csvPhrase.titre_fr = titre_fr.join(' ');
		}
		if (this.checkMotcle) {

			this.checkFilterMotCle();
		} else {
			console.log("getChartOptions2 1");
			this.chartOptions = this.getChartOptions2();
		}





	}

	onChart2Event(e:any){
		console.log("onChart2Event ",e.data);
		this.csvPhrase = e.data.docs[0];
		this.collapsed2 = false;
		let self = this;
		//if (self.phrases[this.csvPhrase.nom] === undefined) {
			console.log("self.phrases[this.csvPhrase.nom] === undefined"); 
			self.phrasesLoading = true;
			self.documentService.listPhrasesByDocWithSeuil(this.csvPhrase.id).subscribe((phrasesData) => {
				const phrases: Phrase[] = [];
				phrasesData.forEach((child) => {
					const phrase: Phrase = new Phrase().deserialize(child);
					phrases.push(phrase);
					return false;
				});

				self.phrases[self.csvPhrase.nom] = phrases;

				self.csvPhrase.phrases = self.phrases[this.csvPhrase.nom].filter(function (value: Phrase, index: number, array: Phrase[]) {
					let ok = false;
					if (self.selectedEnjeux.length > 0) {
						for( const i in self.selectedEnjeux) {
							if (self.selectedEnjeux[i].num === value.enjeu) {
								ok = true;
							}
						}
					} else if (self.checkMotcle) {
						ok = true;
					}
					return ok;
				});
				self.phrasesLoading = false;
				console.log("phrases from listPhrasesByDocWithSeuil: %o", self.csvPhrase.phrases);
			});
		// } else {
		// 	self.csvPhrase.phrases = self.phrases[this.csvPhrase.nom].filter(function (value: Phrase, index: number, array: Phrase[]) {
		// 		let ok = false;
		// 		if (self.selectedEnjeux.length > 0) {
		// 			for( const i in self.selectedEnjeux) {
		// 				if ("#" + self.selectedEnjeux[i].num === value.enjeu) {
		// 					ok = true;
		// 				}
		// 			}
		// 		} else if (self.checkMotcle) {
		// 			ok = true;
		// 		}
		// 		return ok;
		// 	});
		// 	console.log("phrases : %o", self.csvPhrase.phrases);
		// 	self.phrasesLoading = false;
		// }
	}

	getCsvData() {
		const res = [];
		const self = this;
		for (const i in this.csvPhrasesFiltered) {
			let satVal = 0;
			let satKey = "1";
			let enjeuDefined = true;
			const csvPhrase = this.csvPhrasesFiltered[i];
			if (self.selectedEnjeux && self.selectedEnjeux.length > 0) {
				const enj = self.selectedEnjeux[0].num.replace(/#/g, "");
				if (csvPhrase.sat[enj] === undefined) {
					enjeuDefined = false;
				} else {
					for (const s in csvPhrase.sat[enj]) {
						if (parseInt(csvPhrase.sat[enj][s]) > satVal) {
							satVal = parseInt(csvPhrase.sat[enj][s]);
							satKey = s;
						}
					}
				}

			}
			const sat = satKey;
			let color = this.couleurPhrase[sat];

			if (!enjeuDefined) {
				color = "#F44336";
			}





			res.push({
				value: this.csvPhrasesFiltered[i].extraits,
				// name: ""+this.csvPhrasesFiltered[i].phrases.length.toString()+" extraits",
				name: "Extraits",
				itemStyle: { color: color }

			});
		}
		// console.log(this.csvPhrasesFiltered);
		return res;
	}


	checkFilter(filter = "") {
		let isMonde = false;
		// console.log("checkfilter, mot cle", this.checkMotcle, this.motCle);
		this.csvPhrasesFiltered = [];
		this.csvPhrases.forEach((p) => {
			this.csvPhrasesFiltered.push(p);
		});
		console.log('dans checkFilter',this.csvPhrasesFiltered);
		const self = this;
		self.loadingMap = true;
		// setTimeout(function () {
		for (let index = 0; index < this.user.flux.length; index++) {
			const flux: Flux = this.user.flux[index];
			if (flux.selected && flux.group) {
				isMonde = true;
				break;
			}
		}

		for (let index = 0; index < this.user.flux.length; index++) {
			const flux = this.user.flux[index];
			if (this.user.flux[index].isMonde) {
				this.user.flux[index].selected = isMonde;
			}
		}


		// console.log("self.csvPhrasesFiltered %o", self.csvPhrasesFiltered);

		if (self.enjeuOver) {
			if (self.csvPhrase.phrases) {
				self.csvPhrase.phrases = self.phrases[self.csvPhrase.nom] ? self.phrases[self.csvPhrase.nom].filter(function (value: Phrase, index: number, array: Phrase[]) {

					let ok = false;

					if (self.enjeuOver && (value.enjeu === self.enjeuOver.num)) {
						ok = true;
					}
					return ok;

				}) : [];

			}

		} else {
			if (self.selectedEnjeux.length > 0) {
				self.csvPhrasesFiltered.map((csvPhrase, index, array) => {
					csvPhrase.phrases = self.phrases[csvPhrase.nom] ? self.phrases[csvPhrase.nom].filter((phrase: Phrase) => {
						let res = false;
						if (phrase.enjeu === self.selectedEnjeux[0].num) {
							res = true;
						}
						return res;
					}) : [];
					return csvPhrase;
				});


			} else {
				// console.log("here 1")
				self.csvPhrasesFiltered.map((csvPhrase, index, array) => {
					csvPhrase.phrases = self.phrases[csvPhrase.nom] ? self.phrases[csvPhrase.nom] : [];
				});
			}
		}



		if (!this.allFluxFilter) {
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter((a) => {
				let res = false;
				for (const f of self.user.flux) {
					if (f.selected) {
						if (self.cleanString(a.flux) === self.cleanString(f.flux)) {
							res = true;
							return res;
						}

					}
				}

				return res;
			});



			/// sort client_enjeu

			// if(self.client_enjeu && self.client_enjeu.length > 0){
			// 	self.client_enjeu.map((enjeu) => {
			// 		enjeu.count = self.countExtraits.find((elt) => elt.id == enjeu.key).count;
			// 		return enjeu;
			// 	});

			// 	self.enjeux.map((enjeu) => {
			// 		enjeu.count = self.countExtraits.find((elt) => elt.id == enjeu.key).count;
			// 		return enjeu;
			// 	})

			// 	self.client_enjeu.sort((a, b) => b.count - a.count)
			// 	self.enjeux.sort((a, b) => b.count - a.count)
			// }


		}
		if (!this.allFluxFilter && this.test) {
			self.checkMinMaxYear(self.csvPhrasesFiltered);
		}

		if (self.checkPaysFilter()) {
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;
				for (const i in self.newpaysFilter) {
					if (self.newpaysFilter[i]) {
						// for(let j in a.phrases){
						// console.log(a.phrases[j].pays);
						// console.log(a.pays);
						if (a.pays.toLowerCase() === self.pays[i].toLowerCase()) {
							res = true;
							return res;
						}
						// }
					}
				}

				return res;

			});
		}
		// if(self.checkMotcle == false){
		if (self.checkPerfFilter()) {
			console.log("here checkPerfFilter");
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;
				if (a.sat) {
					for (const i in self.perfFilter) {
						if (self.perfFilter[i]) {

							if (self.selectedEnjeux.length > 0 && a.sat[self.selectedEnjeux[0].num.replace(/#/g, "")] && a.sat[self.selectedEnjeux[0].num.replace(/#/g, "")][i.toString()]) {
								res = true;
								break;
							}




						}
					}
				}

				return res;
			});
		}
		// }




		if (self.yearFilter) {
			console.log("here yearFilter");
			this.year_from = "";
			this.year_to = "";
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;

				if (a.date.split("/").length === 3) {
					if (a.date.split("/")[2] === self.yearFilter) {
						// console.log("eto isika yearfilter : ",self.yearFilter);
						res = true;
					}
				}
				return res;
			});
		}

		/*if(self.month_from && self.year_from){
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
				let res = false;
				if(a.date.split('/').length == 3){
					let date  = Date.parse(a.date.split('/')[2]+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
					let date_from = Date.parse(self.year_from+"-"+self.month_from);
					if(date >= date_from){
						res = true;
					}
				}
				return res;
			})

		}

		if(self.month_to && self.year_to){
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
				let res = false;
				if(a.date.split('/').length == 3){
					let date  = Date.parse(a.date.split('/')[2]+"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
					let date_to = Date.parse(self.year_to+"-"+self.month_to);

					if(date <= date_to){
						res = true;
					}
				}
				return res;
			})

		}
*/
		// console.log("self.csvPhrasesFiltered filterd by nexity %o", self.csvPhrasesFiltered.filter((c) => c.nom == "CSV3_0910"));

		if (self.year_from) {
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;
				if (a.date.split("/").length === 3 || a.date.split("-").length === 3) {
					// const sep = (a.date.split("/").length === 3) ? "/" : "-";
					const dateCsv = new Date(a.date);
					// const date = Date.parse(a.date.split(sep)[2]); // +"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
					const date = Date.parse(''+dateCsv.getFullYear()); // +"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
					const date_from = Date.parse(self.year_from);
					if (date >= date_from) {
						res = true;
					}
				}
				return res;
			});
		}

		if (self.year_to) {
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;
				if (a.date.split("/").length === 3 || a.date.split("-").length === 3) {
					// const sep = (a.date.split("/").length === 3) ? "/" : "-";
					const dateCsv = new Date(a.date);
					const date = Date.parse(''+dateCsv.getFullYear());
					// const date = Date.parse(a.date.split(sep)[2]); // +"-"+a.date.split('/')[1]+"-"+a.date.split('/')[0]);
					const date_to = Date.parse(self.year_to);

					if (date <= date_to) {
						res = true;
					}
				}
				return res;
			});
		}



		if (self.paysFilter !== "all") {
			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function (a) {
				let res = false;


				if (a.pays === self.paysFilter) {
					res = true;
				}

				return res;
			});
		}



		// console.log("MOT CLE %o", self.mot_cle);
		// console.log("phrases csv", self.csvPhrasesFiltered);
		if (self.checkMotcle) {
			if (self.motCle || self.motCle2 || self.motCle3) {
				self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter((a) => {
					let res = false;
					let test = false;
					// titre fr
					// if (a.nom.toLowerCase()) {
					// 	if (a.nom.toLowerCase() == self.motCle.toLowerCase()) {
					// 		res = true;
					// 		test = true;
					// 	}
					// }

					// if (a.titre_fr) {
					// 	let mot_test = a.titre_fr.toLowerCase();
					// 	if (mot_test.includes(self.motCle.toLowerCase())) {
					// 		res = true;
					// 		test = true;
					// 	}
					// }

					// if (a.titre_en) {
					// 	let mot_test = a.titre_en.toLowerCase();
					// 	if (mot_test.includes(self.motCle.toLowerCase())) {
					// 		res = true;
					// 		test = true;
					// 	}
					// }

					// phrase fr
					// if(self.translate.currentLang == "fr"){
					if (a.phrases) {
						const re = [];
						for (const i in a.phrases) {

							const mot_test = a.phrases[i].phrase_fr.toLowerCase();
							self.selectedMotsCle.some(
								(clef,indexClef) => {
									if(clef.trim() !== "" && clef !== undefined){
										re[indexClef] = new RegExp("\\s" + clef.toLowerCase() + "\\s");
										if (mot_test.search(re[indexClef]) > 0) {
											res = true;
											test = true;
											return true;
										}
									}
								}
							)
							if(res) break;
							// const re = new RegExp("\\s" + self.motCle.toLowerCase() + "\\s");
							// const re2 = new RegExp("\\s" + self.motCle2.toLowerCase() + "\\s");
							// const re3 = new RegExp("\\s" + self.motCle3.toLowerCase() + "\\s");

							// if (mot_test.search(re) > 0 || mot_test.search(re2) > 0 || mot_test.search(re3) > 0) {
							// 	res = true;
							// 	test = true;
							// 	break;
							// }
						}
					}
					// } else {
					// 	//phrase en
					if (a.phrases) {
						const re = [];
						for (const i in a.phrases) {

							const mot_test = a.phrases[i].phrase_en.toLowerCase();
							self.selectedMotsCle.some(
								(clef,indexClef) => {
									if(clef.trim() !== "" && clef !== undefined){
										re[indexClef] = new RegExp("\\s" + clef.toLowerCase() + "\\s");
										if (mot_test.search(re[indexClef]) > 0) {
											res = true;
											test = true;
											return true;
										}
									}
								}
							)
							if(res) break;
							// const re = new RegExp("\\s" + self.motCle.toLowerCase() + "\\s");
							// const re2 = new RegExp("\\s" + self.motCle2.toLowerCase() + "\\s");
							// const re3 = new RegExp("\\s" + self.motCle3.toLowerCase() + "\\s");

							// if (mot_test.search(re) > 0 || mot_test.search(re2) > 0 || mot_test.search(re3) > 0) {
							// 	res = true;
							// 	test = true;
							// 	break;
							// }
						}
					}
					// }



					return res;
				}).map((csvPhrase) => {
					csvPhrase.phrases = csvPhrase.phrases.filter((phrase) => {
						let res = false;
						let mot_test = phrase.phrase_fr.toLowerCase();
						for(const m in self.selectedMotsCle){
							if (self.selectedMotsCle[m].trim() !== "" && self.selectedMotsCle[m] !== undefined && mot_test.includes(self.selectedMotsCle[m].toLowerCase())) {
								res = true;
								break;
							}
						}
						mot_test = phrase.phrase_en.toLowerCase();
						for(const m in self.selectedMotsCle){
							if (self.selectedMotsCle[m].trim() !== "" && self.selectedMotsCle[m] !== undefined && mot_test.includes(self.selectedMotsCle[m].toLowerCase())) {
								res = true;
								break;
							}
						}
						return res;

					});
					return csvPhrase;
				});
				// console.log(self.csvPhrasesFiltered);
				// console.log("check mot cles finished ,", self.csvPhrasesFiltered);
			}
			else{
				self.csvPhrasesFiltered = [];
			}
		}


		// if(self.mot_cle && self.mot_cle.length > 0){

		// 		// console.log("MOT CLE INSIDE %o", self.mot_cle);

		// 			self.csvPhrasesFiltered = self.csvPhrasesFiltered.filter(function(a){
		// 				let res = false;
		// 				let test = false;
		// 					// titre fr

		// 					for(let i in self.mot_cle){

		// 						let motEntree = self.mot_cle[i].mot;
		// 						console.log("MOT ENTREE %o", motEntree);
		// 						if(a.nom.toLowerCase() == motEntree.toLowerCase()){
		// 							res=true;
		// 							test=true;
		// 						}

		// 							let mot_test = a.titre_fr.toLowerCase();
		// 							if(mot_test.includes(motEntree.toLowerCase())){
		// 								res = true;
		// 								test = true;
		// 							}


		// 						//titre en

		// 						if(!test){
		// 							let mot_test = a.titre_en.toLowerCase();
		// 							if(mot_test.includes(motEntree.toLowerCase())){
		// 								res = true;
		// 								test = true;
		// 							}
		// 						}


		// 						//phrase fr
		// 						if(!test && a.phrases){
		// 							for(let i in a.phrases){

		// 								let mot_test = a.phrases[i].phrase_fr.toLowerCase();
		// 								if(mot_test.includes(motEntree.toLowerCase())){
		// 									res = true;
		// 									test = true;
		// 									break;
		// 								}
		// 							}
		// 						}

		// 						//phrase en
		// 						if(!test && a.phrases){
		// 							for(let i in a.phrases){

		// 								let mot_test = a.phrases[i].phrase_en.toLowerCase();
		// 								if(mot_test.includes(motEntree.toLowerCase())){
		// 									res = true;
		// 									test = true;
		// 									break;
		// 								}
		// 							}
		// 						}
		// 					}







		// 				return res;
		// 			});
		// }

		
		self.loadingMap = false;
		console.log("getChartOptions2 2");
		this.chartOptions = this.getChartOptions2();
		// }.bind(this), 250);
	}


	checkFluxFilter() {
		let res = false;
		for (const f of this.user.flux) {
			if (f.selected) {
				res = true;
				break;
			}
		}


		if (res) {
			const flux = [];
			let index = 0;
			for (const f of this.user.flux) {
				if (f.selected) {
					f.id = index.toString();
					flux.push(f);
				}
				index++;
			}

			// this.db.database.ref("clients/"+this.user.num+"/fluxSelected").set(flux);
			// console.log("SAVE FLUX IN %o","clientFluxSelected/" + this.orga.key + "/" + this.user.num);
			this.db.database.ref("clientFluxSelected/" + this.orga.key + "/" + this.user.num).set(flux);
		} else if (this.edit) {
			// this.db.database.ref("clients/"+this.user.num+"/fluxSelected").set({});
			this.db.database.ref("clientFluxSelected/" + this.orga.key + "/" + this.user.num).set({});
		}
		return res;
	}

	checkPaysFilter() {
		let res = false;
		for (const i in this.newpaysFilter) {
			if (this.newpaysFilter[i]) {
				this.all_pays = "";
				res = true;
				break;
			} else {

				// this.liste_pays = [];
			}
		}
		if (!res) {
			this.all_pays = "Indifférent";
			this.liste_pays = [];
			this.allPaysFilter = true;
		}

		return res;
	}

	checkMot(mot) {
		let res = true;

		// test fr
		for (const i in this.titre_fr) {
			const titre = this.titre_fr[i].toLowerCase();
			const test = titre.includes(mot.toLowerCase());
			if (res) {
				if (test) {
					// this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		// console.log(res);

		// test en
		for (const i in this.titre_en) {
			const titre = this.titre_en[i].toLowerCase();
			const test = titre.includes(mot.toLowerCase());
			if (res) {
				if (test) {
					// this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		// phrase fr
		for (const i in this.phrase_fr) {
			const titre = this.phrase_fr[i].toLowerCase();
			const test = titre.includes(mot.toLowerCase());
			if (res) {
				if (test) {
					// this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		// phrase en
		for (const i in this.phrase_en) {
			const titre = this.phrase_en[i].toLowerCase();
			const test = titre.includes(mot.toLowerCase());
			if (res) {
				if (test) {
					// this.mot_cle.push(mot);
					res = false;
				}
			}
		}

		return res;
	}



	checkPerfFilter() {
		let res = false;
		for (const i in this.perfFilter) {
			if (this.perfFilter[i]) {
				res = true;
				break;
			} else if ((!this.perfFilter[1] && !this.perfFilter[2]) && (!this.perfFilter[3] && !this.perfFilter[4])) {
				this.allPerf = true;
			}
		}
		return res;
	}

	checkMinMaxYear(csvPhrases) {
		// this.options.barDimension = 100;
		this.years = [];
		csvPhrases.map(phrase => {
			this.years.push(phrase.date.split("/")[2]);
		});
		this.years = this.years.filter((v, i) => this.years.indexOf(v) === i && v !== undefined && parseInt(v) > 2000);
		this.years = this.years.sort(function (a, b) { return parseInt(a) - parseInt(b); });

		// this.checkTimelapse();


		const years = this.years.map((function (item) {
			return parseInt(item, 10);
		}));


		//////// timelapse ///////
		if (this.dateFilterToggle === true) {
			this.yearFilter = false;
			const sliderArray = [];
			for (let i = 0; i < years.length; i++) {
				sliderArray.push({ value: years[i], legend: "" });
			}
			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			this.options.stepsArray = sliderArray;
			this.value = Math.min(...years);
			this.highValue = Math.max(...years);
			this.year_from = this.value.toString();
			this.year_to = this.highValue.toString();
			this.dateFilterToggle = false;
			setTimeout(() => {
				this.dateFilterToggle = true;
			}, 50);
		} else {
			const sliderArray = [];
			for (let i = 0; i < years.length; i++) {
				sliderArray.push({ value: years[i] });
			}

			this.options.showTicksValues = false;
			this.options.showTicks = true;
			this.options.hideLimitLabels = true;
			this.options.getPointerColor = ((value: number) => {
				return "#004FA3";
			});

			this.options.getSelectionBarColor = ((value: number) => {
				return "#004FA3";
			});
			// this.options.showTicks = false;
			this.options.stepsArray = sliderArray;

			this.value = Math.min(...years);


			this.yearFilter = this.value.toString();
			this.dateFilterToggle = true;
			setTimeout(() => {
				this.dateFilterToggle = false;
			}, 50);
		}
		this.test = false;
	}
	checkEnjeuFilter() {
		let res = false;
		for (const i in this.enjeux) {
			if (this.enjeux[i].selected) {
				res = true;
				break;
			}
		}
		return res;
	}

	checkFluxUser(flux: string, cons = false) {
		let res = false;
		for (const i in this.user.flux) {
			if (cons) {

				// console.log("'%o' == '%o' : %o",this.user.flux[i].flux.toLowerCase().trim(),flux.toLowerCase().trim(),  this.cleanString(this.user.flux[i].flux) == this.cleanString(flux));

			}

			if (this.cleanString(this.user.flux[i].flux) === this.cleanString(flux)) {
				res = true;
				break;
			}
		}
		return res;
	}

	cleanString(s) {
		if (!s) {
			return "";
		}
		return s.toLowerCase().trim().replace(/[^a-z0-9]/ig, "");
	}

	selectEnjeu(enjeu: Enjeu, e = undefined) {
		// console.log(e);
		console.log("mouse");
		this.checkMotcle = false;
		this.colorBorderMotCle();
		this.closeEnjeux();
		const oldSelectedEnjeux = this.selectedEnjeux.map(data => data.def);
		if (!e || (e && (!e.shiftKey && !e.altKey && !e.ctrlKey && !e.metaKey))) {
			this.selectedEnjeux = [];
		}

		const indexEnjeuInSelected = this.selectedEnjeux.map(data => data.def).indexOf(enjeu.def);

		if (indexEnjeuInSelected !== -1) {
			this.selectedEnjeux.splice(indexEnjeuInSelected, 1);
		} else {
			this.selectedEnjeux.push(enjeu);
		}

		// console.log("this.selectedEnjeux" , this.selectedEnjeux);

		// this.selectedEnjeux.push(enjeu);
		this.enjeuOver = enjeu;
		this.db.database.ref("clientEnjeux/" + this.user.num + "/client_enjeu").set(this.selectedEnjeux);
		this.checkFilter();
		(this.mapToogle === true && this.hide_graph === true && this.hide_map === false) && this.selectTab("chart2");

	}

	selectAllEnjeu() {

		this.checkMotcle = false;
		this.colorBorderMotCle();
		this.closeEnjeux();

		this.db.database.ref("clientEnjeux/" + this.user.num + "/client_enjeu").set(this.selectedEnjeux);
		this.checkFilter();

	}

	countEnjeux() {

	}

	styleEnjeu(enjeu: Enjeu) {
		if (this.checkMotcle === true) {
			this.selectedEnjeux = [];
		}
		if (this.selectedEnjeux) {
			if (this.selectedEnjeux[0]) {
				if (this.selectedEnjeux[0].num === enjeu.num) {
					return "select-first";
				}
			}
			const check = this.selectedEnjeux.find((enj) => enj.num === enjeu.num);
			return (check !== undefined) ? "select-first" : "primary";
		}
		return "primary";

	}

	overEnjeu(enjeu: Enjeu) {
		this.checkMotcle = false;
		this.colorBorderMotCle();
		this.enjeuOver = enjeu;

		this.loadingMap = true;
		this.checkFilter();
	}

	leaveEnjeu(enjeu: Enjeu) {
		this.enjeuOver = undefined;

		this.loadingMap = true;
		this.checkFilter();
	}

	infoEnjeu(content, enjeu: Enjeu) {
		this.enjeuInfo = enjeu;
		this.open(content);
	}

	sideEnjeu() {
		this.sideEnjeux = true;
		// this.collapsed2 = !this.collapsed2;
		this.collapsed3 = false;
		this.collapsedEvent.emit(this.collapsed3);

	}

	closeEnjeux() {
		this.sideEnjeux = false;
		this.collapsed3 = !this.collapsed3;
		this.collapsedEvent.emit(this.collapsed3);
	}
	closeEnjeux2() {
		this.toggleSidebar2();
	}

	addEnjeu(enjeu) {
		const self = this;
		const data = [];
		if (self.selectedEnjeux.length === 0) {
			self.selectedEnjeux.push(enjeu);
		}

		if (self.getClientEnjeuxNumber() < 15) {
			self.db.database.ref("clientEnjeux/" + self.user.num + "/enjeu/" + enjeu.key).set(enjeu).then(() => {
				// SORT ENJEU
				self.client_enjeu.sort((a, b) => b.count - a.count);
				self.enjeux.sort((a, b) => b.count - a.count);
			});

			if (self.enjeu === undefined) {
				self.enjeu = enjeu;
				self.loadingMap = true;
				self.checkFilter();
			}
		}
	}

	checkFilterMotCle() {

		this.enjeuOver = undefined;
		this.selectedEnjeux = [];
		this.checkMotcle = true;

		this.loadingMap = true;
		this.checkFilter();
	}

	colorBorderMotCle() {
		return this.borderMotcle = this.checkMotcle === true ? "1px solid #004fa3" : "1px solid #b8daff";
	}
	checkedMotCle(motCle:number,value:string,e = undefined){
		//une seule selection actif possible parmi les 3 proposées
		// if(e && e.shiftKey){
		// 	if(motCle === 1) this.statMotCle1 = ! this.statMotCle1;
		// 	else if(motCle === 2) this.statMotCle2 = ! this.statMotCle2;
		// 	else if(motCle === 3)	this.statMotCle3 = ! this.statMotCle3;
		// 	if(value){
		// 		if(this.selectedMotsCle.includes(value)){
		// 			this.selectedMotsCle = this.selectedMotsCle.filter(
		// 				(mot) => {
		// 					if(mot !== value){
		// 						return mot;
		// 					} 
		// 				}
		// 			)
		// 		}
		// 		else this.selectedMotsCle.push(value.trim());
		// 	}
		// }
		// else{
			if(motCle === 1){this.statMotCle1 = true;this.statMotCle2 = false;this.statMotCle3 = false}
			else if(motCle === 2){this.statMotCle1 = false;this.statMotCle2 = true;this.statMotCle3 = false}
			else if(motCle === 3){this.statMotCle1 = false;this.statMotCle2 = false;this.statMotCle3 = true}
			this.selectedMotsCle =  (value) ? [value.trim()] : [];
		// }
		this.checkFilter();
	}
	leaveMotCle() {
		this.checkMotcle = false;
		this.loadingMap = true;
		this.checkFilter();
	}

	searchWord(input:number,mot) {

		this.checkMotCleExist = mot;
		const motCleOrder = input === 3 ? input : (input === 2 ? input : "");
		if (mot && mot !== "") {
			this.checkMotcle = true;
			this.colorBorderMotCle();
			this.selectedEnjeux = [];
			this.enjeuOver = undefined;
			// this.db.database.ref("clients/" + this.user.num + "/mots_cles"+motCleOrder).set(mot.toLowerCase());
			if(input === 1) this.motCle = mot;
			else if (input === 2) this.motCle2 = mot;
			else if (input === 3) this.motCle3 = mot;
			this.selectedMotsCle = [mot];
			this.loadingMap = true;
			this.checkFilter();
		}

		// console.log(mot.length);
		// if(mot != undefined && mot != null && mot.length > 0){
		// 	let res = true;

		// 	this.db.database.ref("clients/"+this.user.num+"/mots_cles").push({mot:mot.toLowerCase()});

		// 	//test fr
		// 	for(let i in this.titre_fr){
		// 		let titre = this.titre_fr[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//test en
		// 	for(let i in this.titre_en){
		// 		let titre = this.titre_en[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//phrase fr
		// 	for(let i in this.phrase_fr){
		// 		let titre = this.phrase_fr[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//phrase en
		// 	for(let i in this.phrase_en){
		// 		let titre = this.phrase_en[i].toLowerCase();
		// 		let test = titre.includes(mot.toLowerCase());
		// 		if(res){
		// 			if(test){
		// 				//this.mot_cle.push(mot);
		// 				res = false;
		// 			}
		// 		}
		// 	}

		// 	//if(!res){
		// 		this.loadingMap = true;
		// 		this.checkFilter();
		// 	//}
		// } else {
		// 	this.loadingMap = true;
		// 	this.checkFilter();
		// }

	}

	deleteMotCles(motkey) {
		// console.log(motkey);
		const self = this;
		self.db.database.ref("clients/" + self.user.num + "/mots_cles/" + motkey).remove();
		self.mot_cle = self.mot_cle.filter((value, index) => {
			return index !== motkey;
		});
		self.motEntree = undefined;
		this.loadingMap = true;
		this.checkFilter();
	}

	deleteEnjeuClient(key, enjeu) {

		const self = this;

		self.db.database.ref("clientEnjeux/" + self.user.num + "/enjeu/" + enjeu.key).once("value", function (data) {
			if (data.exists()) {
				const check = self.selectedEnjeux.find((enj) => enj.num === data.val().num);
				console.log("HERE CHECK %o", check);
				if (check) {
					self.selectedEnjeux = [];
					self.db.database.ref("clientEnjeux/" + self.user.num + "/client_enjeu").set(self.selectedEnjeux);

				}
				console.log("HERE self.selectedEnjeux  %o", self.selectedEnjeux);
				console.log("delete enjeu %o", enjeu);
				self.db.database.ref("clientEnjeux/" + self.user.num + "/enjeu/" + enjeu.key).remove();
				self.checkFilter();
			}

		});






	}

	selectCsvPhrase2(csv: CsvPhrase) {
		this.csvClick = true;
		this.sideEnjeux = false;
		this.csvPhrase = csv;
		this.csvSelected = csv.nom;
		const self = this;

		// if(self.phrases[csv.nom] == undefined){
		// 	let ref = self.db.object("phrases_csv/"+csv.nom).snapshotChanges().subscribe(function(phrasesSnap){
		// 		ref.unsubscribe();
		// 		let phrases:Phrase[] = [];
		// 		phrasesSnap.payload.forEach(function(child){
		// 			phrases.push(new Phrase().deserialize(child.val()));
		// 			return false;
		// 		});

		// 		self.csvPhrase.phrases = phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
		//            let perf = self.getMaxPerfPhrase(value).perf;
		//            let ok = false;
		//            if(self.selectedEnjeux.length > 0){
		//            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);
		// 			if(check != undefined){
		// 				ok = true;
		// 			}
		//            }else{
		//            	ok = true;
		//            }


		//            return (perf >= self.seuil) && ok;

		//          })
		// 	});
		// }else{
		// 	self.csvPhrase.phrases = self.phrases[csv.nom].filter(function(value:Phrase, index:number, array:Phrase[]){
		//            let perf = self.getMaxPerfPhrase(value).perf;
		//            let ok = false;
		//            if(self.selectedEnjeux.length > 0){
		//            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);

		// 			if(check != undefined){
		// 				ok = true;
		// 			}
		//            }else{
		//            	ok = true;
		//            }


		//            return (perf >= self.seuil) && ok;

		//          });
		// }
		self.loading = false;



	}

	selectCsvPhrase(csv: CsvPhrase) {
		this.csvClick = true;
		this.sideEnjeux = false;
		this.csvPhrase = csv;
		this.csvSelected = csv.nom;
		const self = this;
		// if(self.phrases[csv.nom] == undefined){
		// 	let ref = self.db.object("phrases_csv/"+csv.nom).snapshotChanges().subscribe(function(phrasesSnap){
		// 		ref.unsubscribe();
		// 		let phrases:Phrase[] = [];
		// 		phrasesSnap.payload.forEach(function(child){
		// 			phrases.push(new Phrase().deserialize(child.val()));
		// 			return false;
		// 		});

		// 		self.csvPhrase.phrases = phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
		//            let perf = self.getMaxPerfPhrase(value).perf;
		//            let ok = false;
		//            if(self.selectedEnjeux.length > 0){
		//            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);
		// 			if(check != undefined){
		// 				ok = true;
		// 			}
		//            }else{
		//            	ok = true;
		//            }


		//            return (perf >= self.seuil) && ok;

		//          })
		// 	});
		// }else{
		// 	self.csvPhrase.phrases = self.phrases[csv.nom].filter(function(value:Phrase, index:number, array:Phrase[]){
		//            let perf = self.getMaxPerfPhrase(value).perf;
		//            let ok = false;
		//            if(self.selectedEnjeux.length > 0){
		//            	let check = self.selectedEnjeux.find((enj) => enj.num == value.enjeu);

		// 			if(check != undefined){
		// 				ok = true;
		// 			}
		//            }else{
		//            	ok = true;
		//            }


		//            return (perf >= self.seuil) && ok;

		//          });
		// }





		self.collapsed2 = false;
		self.collapsedEvent.emit(self.collapsed2);




	}

	selectTab(type: string) {
		if(type === "map"){
			this.mapToogle = true;
			this.hide_graph = true;
			this.hide_map = false;

			let csv = [];
			let csvMonde = [];
			let csvNonMonde = [];
			const enjeuxArray = (!this.checkMotcle) ? this.selectedEnjeux : this.allEnjeux; ;
			for(const i in this.user.flux){
				if (this.user.flux[i].selected && !this.user.flux[i].isMonde && this.user.flux[i].flux.toLowerCase() !== "monde") {
					// console.log('les flux valide',this.user.flux[i].flux);
				}
				let ok = false;
				if (this.user.flux[i].isMonde) {
					continue;
				}
				if (this.user.flux[i].selected) {
					ok = true;
				}
				if (!this.checkFluxFilter()) {
					ok = true;
				}
				if (ok) {
					const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
						let res = false;
						if (this.cleanString(a.flux) === this.cleanString(this.user.flux[i].flux)) {
							res = true;
						}
						return res;
					});
					for(const idEnj in enjeuxArray){
						for (const c in csvPhrases) {
							if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
								if(!csvNonMonde.find(csv => csv.nom === csvPhrases[c].nom)){
									csvPhrases[c].phrases = this.phrases[csvPhrases[c].nom].filter( (value: Phrase) => {
										if (this.selectedEnjeux.length > 0) {
											for(const i in this.selectedEnjeux){
												// if ("#" + this.selectedEnjeux[i].num === value.enjeu) {
												if ( this.selectedEnjeux[i].num === value.enjeu) {
														return true;
												}
											}
										}
									});
									if(csvPhrases[c].phrases.length){
										csvNonMonde.push(
											{
												nom: csvPhrases[c].nom,
												csv:csvPhrases[c],
												lat:csvPhrases[c].lat,
												long:csvPhrases[c].long,
												color: this.getColor(csvPhrases[c].flux)
											}
										)
									}
								} 
							}
						}
					}
				}
			}
			csv = csv.concat(csvNonMonde);

			for(const i in this.user.flux){
				let ok = false;
				if (this.user.flux[i].isMonde && this.mondeSelected) {
					ok = true;
				}
				if(ok){
					const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
						let res = false;
						if (this.slug(a.flux) === this.slug(this.user.flux[i].flux)) {
							res = true;
						}
						return res;
					});
					for(const idenj in enjeuxArray) {
						for (const c in csvPhrases) {
							if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
								if (enjeuxArray[idenj] && csvPhrases[c].enjeu_2_color && csvPhrases[c].enjeu_2_color["#" + enjeuxArray[idenj].key]) {}
								else if (!this.checkMotcle)continue;
								
								else {
									if (csvPhrases[c].nom === this.csvSelected) {}
								}
								if(!csvMonde.find(csv => csv.nom === csvPhrases[c].nom)){
									csvPhrases[c].phrases = this.phrases[csvPhrases[c].nom].filter( (value: Phrase) => {
										if (this.selectedEnjeux.length > 0) {
											for(const i in this.selectedEnjeux){
												// if ("#" + this.selectedEnjeux[i].num === value.enjeu) {
												if (this.selectedEnjeux[i].num === value.enjeu) {
														return true;
												}
											}
										}
									});
									if(csvPhrases[c].phrases.length){
										csvMonde.push(
											{
												nom: csvPhrases[c].nom,
												csv:csvPhrases[c],
												lat:csvPhrases[c].lat,
												long:csvPhrases[c].long,
												color: this.getColor('MONDE')
											}
										)
									}
								}
							}
						}
					}
				}
			}
			csv = csv.concat(csvMonde);
			this.csvPhrasesFilteredMap = csv;
		}
		else if(type === "chart2"){
			this.mapToogle = true;
			this.hide_graph = true;
			this.hide_map = false;

			// console.log('chartoptions',this.chartOptions);
			
			let option;
			option = {
				tooltip: {
					position: function(point, params, dom, rect, size){
						if(params.data.value[0] === 0 || params.data.value[0] === 1 || params.data.value[0] === 2 || params.data.value[0] === 3 || params.data.value[0] === 4)
							return 'right';
						else
							return 'left';
					},
					formatter: function(params){
						// let legendOnHover = `Catégorie : ${params.data.categorie}<br />`;
						let legendOnHover = '';
						params.data.docs.forEach(
							(doc:any) => {
								// let titre_fr = doc.titre_fr.split('_');
								// if(titre_fr.length >=2 ){
								// 	titre_fr.splice(0,1);
								// }
								let titre =  doc.titre_fr.split(' ');
								let text = [];
								let count = 0;
								titre.map(
									(t,i) => {
										if(count === 3){text.push('<br />');count = 0;}count++;text.push(t); // tout les tois mots retour à la llgne
									}
								)
								legendOnHover += `${ text.join(' ') } : ${doc.phrases.length} extraits<br />`;
							}
						);
						return legendOnHover;
					}
				},
				title: [],
				singleAxis: [],
				series: []
			};


			let years = [];
			this.years.map(
				y => {
					if(y >= this.year_from && y <= this.year_to){
						years.push(`${y}`);
						years.push(`${y}b`);
						years.push(`${y}c`);
						years.push(`${y}d`);
						years.push(`${y}e`);
					}
				}
			)
			let organisations = [];
			this.user.flux.map(
				f => {
					(!f.isMonde && f.selected && f.flux.trim().toLowerCase() != "monde") && organisations.push(f.flux.toUpperCase().trim());
				}
			)
			const csvPhrases = this.chartOptions.series[1].data2;
			let taille = 10;
			organisations.forEach(o => taille += 10);
			organisations.forEach( (orga,idx) => {
				
				option.title.push({
					// textBaseline: 'middle',
					// top: (idx + 0.5) * 100 / organisations.length + '%',
					// text: orga.substring(0, 6),
					show: false
				});
				option.singleAxis.push({
					left: 50,
					// right: 30,
					type: 'category',
					boundaryGap: false,
					data: years,
					top: (idx * taille / organisations.length + 5) + '%',
					height: (taille / organisations.length - 10) + '%',
					axisLabel: {
						show: (idx === (organisations.length - 1)) ? true : false,
						interval: 4
					},
					// minorTick: { show: false },
					axisLine: {  show: false },
					axisTick: {  show: false },  
					// splitArea: {  show: false },
					splitLine: {  show: false },
					// axisPointer: {  show: false },						
					// minorSplitLine:{  show: false },
				});
				option.series.push({
					singleAxisIndex: idx,
					coordinateSystem: 'singleAxis',
					type: 'scatter',
					data: [],
					symbolSize: function (dataItem) {
						return dataItem[1] * 4;
					},
					itemStyle:{ color: this.getColor(orga) }
				});
			} )
			// let dataCategorie = [{value:[0,0,0],csv:[],categorie:8}];
			let dataCategorie = [];
			let csvMonde = [];
			let csvNonMonde = [];
			const enjeuxArray = (!this.checkMotcle) ? this.selectedEnjeux : this.allEnjeux; ;
			for(const i in this.user.flux){
				
				let ok = false;
				if (this.user.flux[i].isMonde) {
					continue;
				}
				if (this.user.flux[i].selected) {
					ok = true;
				}
				if (!this.checkFluxFilter()) {
					ok = true;
				}
				if (ok) {
					const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
						let res = false;
						if (this.cleanString(a.flux) === this.cleanString(this.user.flux[i].flux)) {
							res = true;
						}
						return res;
					});
					for(const idEnj in enjeuxArray){
						for (const c in csvPhrases) {
							if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
								if(!csvNonMonde.find(csv => csv.nom === csvPhrases[c].nom)){ 
									csvPhrases[c].phrases = this.phrases[csvPhrases[c].nom].filter( (value: Phrase) => {
										if (this.selectedEnjeux.length > 0) {
											for(const i in this.selectedEnjeux){
												// if ("#" + this.selectedEnjeux[i].num === value.enjeu) {
												if (this.selectedEnjeux[i].num === value.enjeu) {
														return true;
												}
											}
										}
									});
									let titre_en = csvPhrases[c].titre_en.split('_');
									if(titre_en.length >=2 ){
										titre_en.splice(0,1);
									}
									csvPhrases[c].titre_fr = titre_en.join(' ');
									let titre_fr = csvPhrases[c].titre_en.split('_');
									if(titre_fr.length >=2 ){
										titre_fr.splice(0,1);
									}
									csvPhrases[c].titre_fr = titre_fr.join(' ');
									csvNonMonde.push(csvPhrases[c]);
								}
							}
						}
					}
				}
			}
			
			
			console.log('les csv Non Monde',csvNonMonde);
			// console.log('les annee',this.years);
			// console.log('axe X',years);
			// csvNonMonde = csvNonMonde.map(
			// 	d => {
			// 		return {...d, categorie: Math.floor(Math.random() * 7) + 1 }
					
			// 	}
			// );
			organisations.forEach(
				(orga,index) => {
					for(const idCsv in csvNonMonde){
						
						if(this.cleanString(csvNonMonde[idCsv].flux) === this.cleanString(orga)){
							let param2 = 0;
							let param3 = 0;
							const dateCsv = new Date(csvNonMonde[idCsv].date);
							const year = ''+dateCsv.getFullYear();
							
							// years.some(
							this.years.map(
								(y,i) => {
									// if(y.substring(0, 4) === year){
									if(y == year){
										const cat = parseInt(csvNonMonde[idCsv].categorie);
										if(cat === 1 || cat === 2 || cat === 3){
											years.map((a,indexA) =>{ 
												if(a == y ){
														param2 = indexA;
													}
												}
											);
											param3 = (cat === 1) ? 8 : ((cat === 2) ? 6 : 4) ;
										}
										else if(cat === 4){
											years.map((a,indexA) =>{ 
												if(a == y ){
														param2 = indexA + 1;
													}
												}
											);
											param3 = this.filterCat && 2;
										}
										else if(cat === 5){
											years.map((a,indexA) =>{ 
												if(a == y ){
														param2 = indexA + 2;
													}
												}
											)
											param3 = this.filterCat && 2;
										}
										else if(cat === 6){
											years.map((a,indexA) =>{ 
												if(a == y ){
														param2 = indexA + 3;
													}
												}
											)
											param3 = this.filterCat && 2;
										}
										else if(cat === 7){
											years.map((a,indexA) =>{ 
												if(a == y ){
														param2 = indexA + 4;
													}
												}
											)
											param3 = this.filterCat && 2;
										}
										// return true; 
									}
								}
							)
							let checkExist = dataCategorie.find(d => d.value[0] === index && d.value[1] === param2 );
							if(checkExist){
								// checkExist.csv = checkExist.csv.concat([csvNonMonde[idCsv]]);
							}
							else{
								dataCategorie.push({value:[index,param2,param3],csv:[csvNonMonde[idCsv]],categorie:csvNonMonde[idCsv].categorie});
							}
							// if(dataTemp.find(d => JSON.stringify(d.a) === JSON.stringify([index,param2]))){
							// 	let dataCatCur = dataCategorie.find(d => d.value[0] === index && d.value[1] === param2 );
							// 	dataCatCur.csv = dataCatCur.csv.concat([csvNonMonde[idCsv]]);
							// 	break;
							// }
							// dataTemp.push({a:[index,param2]});

						}
					}
				}
			)
			// console.log('dataCat',dataCategorie);
			// console.log('dataTemp',dataTemp);
			// console.log('dataTemp2',dataTemp2);
			/**
			 * décommenté si vouloir faire apparaitre donnée monde Monde
			 */
			// for(const i in this.user.flux){
			// 	let ok = false;
			// 	if (this.user.flux[i].isMonde && this.mondeSelected) {
			// 		ok = true;
			// 	}
			// 	if(ok){
			// 		const csvPhrases = this.csvPhrasesFiltered.filter( (a) => {
			// 			let res = false;
			// 			if (this.slug(a.flux) === this.slug(this.user.flux[i].flux)) {
			// 				res = true;
			// 			}
			// 			return res;
			// 		});
			// 		for(const idenj in enjeuxArray) {
			// 			for (const c in csvPhrases) {
			// 				if (this.cleanString(csvPhrases[c].flux) === this.cleanString(this.user.flux[i].flux)) {
			// 					if (enjeuxArray[idenj] && csvPhrases[c].enjeu_2_color && csvPhrases[c].enjeu_2_color["#" + enjeuxArray[idenj].key]) {}
			// 					else if (!this.checkMotcle)continue;
								
			// 					else {
			// 						if (csvPhrases[c].nom === this.csvSelected) {}
			// 					}
			// 					if(!csvMonde.find(csv => csv.nom === csvPhrases[c].nom)){ 
			// 						csvPhrases[c].phrases = this.phrases[csvPhrases[c].nom].filter( (value: Phrase) => {
			// 							if (this.selectedEnjeux.length > 0) {
			// 								for(const i in this.selectedEnjeux){
			// 									if ("#" + this.selectedEnjeux[i].num === value.enjeu) {
			// 											return true;
			// 									}
			// 								}
			// 							}
			// 						});
			// 						csvMonde.push(csvPhrases[c]);
			// 					}	
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			
			// for(const idCsv in csvMonde){
			// 	let param1 = organisations.length - 1;
			// 	let param2 = 0;
			// 	let param3 = 0;
			// 	let dataTemp = [];
			// 	const dateCsv = new Date(csvMonde[idCsv].date);
			// 	const year = ''+dateCsv.getFullYear();
			// 	years.some(
			// 		(y,i) => {
			// 			if(y.substring(0, 4) === year){
			// 				const cat = parseInt(csvMonde[idCsv].categorie);
			// 				if(cat === 1 || cat === 2 || cat === 3){
			// 					param2 = i;
			// 					param3 = (cat === 1) ? 8 : ((cat === 2) ? 5 : 3) ;
			// 				}
			// 				else if(cat === 4){
			// 					param2 = i+1;
			// 					param3 = 1;
			// 				}
			// 				else if(cat === 5){
			// 					param2 = i+2;
			// 					param3 = 1;
			// 				}
			// 				else if(cat === 6){
			// 					param2 = i+3;
			// 					param3 = 1;
			// 				}
			// 				else if(cat === 7){
			// 					param2 = i+4;
			// 					param3 = 1;
			// 				}
			// 				return true;
			// 			}
			// 		}
			// 	)
			// 	if(dataTemp.find(d => JSON.stringify(d.a) === JSON.stringify([param1,param2]))){
			// 		let dataCatCur = dataCategorie.find(d => d.value[0] === param1 && d.value[1] === param2 );
			// 		dataCatCur.csv = dataCatCur.csv.concat([csvNonMonde[idCsv]]);
			// 		break;
			// 	}
			// 	dataTemp.push({a:[param1,param2]});
			// 	// if(dataCategorie.find(d => d.value === [param1,param2,param3] )){
			// 	// 	let dataCatCur = dataCategorie.find(d => d.value === [param1,param2,param3] );
			// 	// 	dataCatCur.csv = dataCatCur.csv.concat([csvNonMonde[idCsv]]);
			// 	// 	break;	
			// 	// }
			// 	dataCategorie.push({value:[param1,param2,param3],csv:[csvMonde[idCsv]],categorie:csvMonde[idCsv].categorie});
			// }
			// console.log('csvMonde',csvMonde);
			/** fin: vouloir apparaitre monde */
			dataCategorie.forEach(function (dataItem) {
				console.log('dataItem dans scartter = ',dataItem);
				option.series[dataItem.value[0]].data.push({value:[dataItem.value[1], dataItem.value[2]],docs:dataItem.csv,categorie:dataItem.categorie});
			});
			console.log('config générale',option);


			this.chartOptions2 = option;
			

		}
		else if(type === "chart"){
			this.mapToogle = false;
			this.hide_graph = false;
			this.hide_map = true;

		}
		// switch (type) {
		// 	case "map":
		// 		this.mapToogle = true;
		// 		this.hide_graph = true;
		// 		this.hide_map = false;
		// 		break;
		// 	case "chart":
		// 		this.mapToogle = false;
		// 		this.hide_graph = false;
		// 		this.hide_map = true;

		// 		break;
		// 	default:
		// 		// code...
		// 		break;
		// }

		// this.checkFilter();



		// console.log("mapToogle %o", this.mapToogle);
	}

	toogleFluxLimit() {
		if (this.fluxLimit === 3) {
			this.fluxLimit = this.user.flux.length;
		} else {
			this.fluxLimit = 3;
		}
	}

	fluxColor(f: any) {

		const style: any = {
			"color": this.getColor(f.flux),
			"font-size": "0.8em",
			"position": "relative",
			"top": "-1px"
		};

		return style;

	}

	showFlux(flux: Flux) {
		let monde = false;
		this.mondeFlux.forEach((f) => {
			if (this.cleanString(flux.flux) === this.cleanString(f.flux)) {
				monde = true;
			}
		});
		return monde ? { "display": "none" } : {};
	}

	infoDetailStyle() {
		jQuery(".info-detail").scrollTop(0);
		const h = jQuery(".info-csv").height();
		const nh: number = 590 - h;
		const style = {
			"height": nh.toString() + "px",
			"overflow-y": "scroll"
		};
		return style;
	}

	addEnjeuxStyle() {
		const style = {
			"padding-left": "57px",
			"overflow": "scroll"

		};
		if (this.countEnjeu < 4) {
			style["max-height"] = "75%";
		} else if (this.countEnjeu < 8) {
			style["max-height"] = "65%";
		} else if (this.countEnjeu < 12) {
			style["max-height"] = "56%";
		} else if (this.countEnjeu >= 12) {
			style["max-height"] = "47%";
		}
		return style;


	}
	// phraseStyle(sat) {
	// 	if (sat == "0") {
	// 		sat = "1";
	// 	}
	// 	let style = {
	// 		"border-left": "5px solid " + this.couleurPhrase[sat],
	// 		"border-radius": "3px",
	// 		"margin-top": "20px"
	// 	}
	// 	return style;
	// }

	phraseStyle(enjeu_2_color) {
		if (!enjeu_2_color) {
			enjeu_2_color = "constat";
		}
		const style = {
			"border-left": "5px solid " + ((this.checkMotcle) ? this.getColor("uncategorized") : this.getColor(enjeu_2_color)),
			"border-radius": "3px",
			"margin-top": "20px"
		};
		return style;
	}
	markerStyle(color: string){
		return { "color":color, "fill":color }
	}

	makerStyle(csv) {
		let color = "#eee";



		// color = this.getColor(csv.flux);
		color = csv;
		const style: any = {
			"color": color,
			"fill": color
		};

		// if(csv.nom == this.csvSelected){
		// 	style = {
		// 		"color": color,
		// 		"fill": color,
		// 		"height": "34px !important",
		// 		"width": "34px !important",
		// 		"position": "relative !important",
		// 		"top": "-14px !important",
		// 	}

		// }

		return style;
	}

	lang(mot: string) {
		if (mot) {
			mot = mot.toLowerCase().replace(/\s+$/, "");

			return this.traduction[mot] !== undefined ? this.traduction[mot][this.translate.currentLang] : mot;
		}
		return "";
	}

	fr(mot) {
		return this.traduction[mot] !== undefined ? this.traduction[mot]["fr"] : mot;
	}

	en(mot) {
		return this.traduction[mot] !== undefined ? this.traduction[mot]["en"] : mot;
	}

	getColor(text: string) {
		if (!text) {
			text = "undefined";
		}
		if (text === "uncategorized") {
			text = "constat";
		}

		text = text.toLowerCase().trim();

		return this.couleurs ? (this.couleurs[text] ? this.couleurs[text] : "#868E96") : "#868E96";
	}

	styleLegend(text) {
		const style = {
			"font-size": "0.8em",
			"color": this.getColor(text)
		};
		return style;
	}

	getColorIcon(text: string) {

		text = text.toLowerCase();
		return this.couleurs ? (this.couleurs[text] ? this.couleurs[text] : "#868E96") : "#868E96";
	}

	handleSVG(svg: SVGElement, parent: Element | null): SVGElement {
		// console.log('Loaded SVG: ', svg.childNodes, parent);
		svg.setAttribute("fill", "white");

		// svg.firstChild.textContent = svg.firstChild.textContent.replace(/000000/g, "ff0000");
		// svg.firstChild.textContent = svg.firstChild.textContent.replace(/000/g, "ff0000");
		// console.log("after %o",svg.firstChild.textContent);
		//    svg.childNodes.forEach(function(item){
		//    	if(item.type){
		//    		item.setAttribute('fill', 'white');
		//    		item.setAttribute('stroke', 'white');
		//    	}
		//    	console.log(item.type);
		//     // item.setAttribute('fill', 'white');
		// });
		return svg;
	}


	getMaxPerfPhrase(phrase: Phrase) {
		// console.log("getting max perf");

		let enjeu: Enjeu;
		let perf = 0;
		const perfSat = 0;

		for (const i in this.enjeux) {

			const cur = this.computePerf(phrase, this.enjeux[i]);

			if (cur > perf) {
				enjeu = this.enjeux[i];
				perf = cur;
			}
		}

		// perfSat = this.computeSatPerf(phrase).points;

		return { "perf": perf, "enjeu": enjeu, "perfSat": perfSat };
	}

	computePerf(phrase: Phrase, enjeu: Enjeu) {
		// mettre phrase en minuscule
		if (phrase) {
			const phrase_fr = phrase.phrase_fr ? (" " + phrase.phrase_fr.toLowerCase() + " ") : "";
			const phrase_en = phrase.phrase_en ? (" " + phrase.phrase_en.toLowerCase() + " ") : "";

			let perf_fr = 0;
			let perf_en = 0;
			// fr
			if (phrase.langue && phrase.langue.toLowerCase() === "fr") {
				for (const i in enjeu.mots.fr) {
					const reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
					if (phrase_fr.search(reg) >= 0) {
						perf_fr += enjeu.mots.fr[i].point;

					}
				}
			} else {
				// en
				for (const i in enjeu.mots.en) {
					const reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
					if (phrase_en.search(reg) >= 0) {
						perf_en += enjeu.mots.en[i].point;

					}
				}
			}




			return (perf_fr > perf_en) ? perf_fr : perf_en;
		} else {
			return 0;
		}


	}

	computeCsvSat(phrases: Phrase[], enjeu: Enjeu) {
		const sat = [];
		for (const p in phrases) {
			const phrase = phrases[p];

			if (phrase.enjeu === enjeu.num) {

				if (sat[phrase.satisfaction] === undefined) {
					sat[phrase.satisfaction] = 1;
				} else {
					sat[phrase.satisfaction] += 1;
				}
			}
		}
		let res = "1";
		let cur = 0;
		for (const i in sat) {
			if (sat[i] > cur) {
				cur = sat[i];
				res = i;
			}
		}

		return res;
	}

	computeCsvSat2(sats: any[], enjeu: Enjeu) {
		let sat = [];
		for (const p in sats) {
			const s = sats[p];

			if (p === enjeu.num.replace(/#/g, "")) {

				sat = sats[p];
				break;
			}
		}
		let res = "0";
		let cur = 0;
		for (const i in sat) {
			if (sat[i] > cur) {
				cur = sat[i];
				res = i;
			}
		}

		return res;
	}

	open(content) {
		const options: NgbModalOptions = (this.user.app_1.trim().toLowerCase() !== "discours" && this.user.app_2.trim().toLowerCase() !== "discours") ? {size: "lg", centered: true,backdrop: 'static',keyboard: false } : { size: "lg", centered: true  };
		this.modalService.open(content, options).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return "by pressing ESC";
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return "by clicking on a backdrop";
		} else {
			return `with: ${reason}`;
		}
	}

	public slug(str) {
		str = str.replace(/^\s+|\s+$/g, ""); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		const from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
		const to = "aaaaaeeeeeiiiiooooouuuunc------";
		for (let i = 0, l = from.length; i < l; i++) {
			str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, "") // remove invalid chars
			.replace(/\s+/g, "-") // collapse whitespace and replace by -
			.replace(/-+/g, "-"); // collapse dashes

		return str;
	}

	alert(text) {
		alert(text);
	}


	receiveCollapsed($event) {
		this.collapedSideBar = $event;
	}

	eventCalled() {
		this.isActive = !this.isActive;
	}

	eventCalled2() {
		this.isActive2 = !this.isActive2;
	}

	eventCalled3() {
		this.isActive3 = !this.isActive3;
	}

	addExpandClass(element: any) {
		if (element === this.showMenu) {
			this.showMenu = "0";
		} else {
			this.showMenu = element;
		}
	}

	toggleCollapsed() {
		this.csvClick = false;
		this.collapsed = !this.collapsed;
		this.collapsedEvent.emit(this.collapsed);
	}

	toggleCollapsed2() {
		this.csvClick = false;
		this.collapsed2 = !this.collapsed2;
		this.collapsedEvent.emit(this.collapsed2);
	}

	toggleCollapsed3() {
		this.sideEnjeux = true;
		this.collapsed3 = false;
		this.collapsed = true;
		this.collapsedEvent.emit(this.collapsed3);
		const dom: any = document.querySelector("body");
		dom.classList.toggle(this.pushRightClass);
	}

	isToggled(): boolean {
		const dom: Element = document.querySelector("body");
		return dom.classList.contains(this.pushRightClass);
	}

	isToggledEnjeu(): boolean {
		const dom: Element = document.querySelector("body");
		return dom.classList.contains(this.pushLeftClass);
	}

	toggleSidebar() {
		const dom: any = document.querySelector("body");
		dom.classList.toggle(this.pushRightClass);
	}
	toggleSidebar2() {
		this.csvClick = false;
		this.sideEnjeux = false;
		this.collapsed3 = true;
		this.collapsed = false;
		this.collapsedEvent.emit(this.collapsed);
		const dom: any = document.querySelector("body");
		dom.classList.toggle(this.pushRightClass);
	}

	toggleSidebar3() {
		// this.csvClick = true;
		this.sideEnjeux = true;
		const dom: any = document.querySelector("body");
		dom.classList.toggle(this.pushRightClass);
	}

	toggleSidebarEnjeu() {
		this.collapsed3 = false;
		this.collapsedEvent.emit(this.collapsed3);
	}

	rltAndLtr() {
		const dom: any = document.querySelector("body");
		dom.classList.toggle("rtl");
	}

	changeLang(language: string) {
		this.translate.use(language);
	}

	onLoggedout() {
		localStorage.removeItem("isLoggedin");
	}

	checkEnjeu(idEnjeu, client_enjeu) {
		const self = this;
		let test = true;

		for (const i in client_enjeu) {
			if (test) {
				if (client_enjeu[i].key === idEnjeu.toString()) {
					test = false;
					break;
				}
			}
		}
		return test;
	}

	testvalue(def) {
		let test = true;
		if (test) {
			if (def === 0) {

				test = false;
			}
		}
		return test;
	}

	showCheckboxes() {
		if (this.showCheck) {
			this.showCheck = false;
		} else {
			this.showCheck = true;
		}
	}

	deplace_left(csv) {

		const self = this;

		for (let i = 0; i < this.csvPhrases.length; i++) {
			if (self.csvPhrases[i].nom === csv.nom) {
				const sum = i + 1;
				// let min = i-1;
				// if(self.csvPhrases[sum] == undefined){
				// 	this.selectCsvPhrase2(self.csvPhrases[min]);
				// } else if(self.csvPhrases[min] == undefined){
				// 	this.selectCsvPhrase2(self.csvPhrases[sum]);
				// }/* else {
				// 	this.toggleCollapsed2();
				// }*/

				if (self.csvPhrases[sum] !== undefined) {
					this.csvPhrase = self.csvPhrases[sum];
					this.selectCsvPhrase2(self.csvPhrases[sum]);
				}
			}
		}
	}

	deplace_right(csv) {
		const self = this;
		const test2 = true;
		for (let i = 0; i < this.csvPhrases.length; i++) {
			if (self.csvPhrases[i].nom === csv.nom) {
				const min = i - 1;
				if (self.csvPhrases[min] !== undefined) {
					this.csvPhrase = self.csvPhrases[min];
					this.selectCsvPhrase2(self.csvPhrases[min]);
				}
			}
		}
	}

	selectMotCle() {
		this.hideEnjeu = true;
		console.log(this.selectedMotsCle);
		this.checkFilterMotCle();
	}

	imageExists(image_url) {

		const http = new XMLHttpRequest();

		http.open("HEAD", image_url, false);
		http.send();

		return http.status !== 404;

	}
	toggleFilterShow(){
		return this.showFilterIcon = true;
	}
	toggleFilterHide(){
		return this.showFilterIcon = false;
	}
	setOpacityIcon(){
		return this.showFilterIcon ? { 'opacity' : '0.9' } : { 'opacity' : '0.0' };
	}
	setOpacityEyeIcon(){
		return this.showEyeIcon ? { 'opacity' : '0.9' } : { 'opacity' : '0.0' }; 
	}
	toggleEyeHide(){
		this.showEyeIcon = false;
	}
	toggleEyeShow(){
		this.showEyeIcon = true;
	}
	etiquetteGraphe(){
		this.showEtiquetteGrap = !this.showEtiquetteGrap;
		this.checkFilter();
	}
	filterCategorie(){
		this.filterCat = !this.filterCat; 
		this.selectTab('chart2');
		console.log('activé filtre categorie');
	}
	getPictoSvg(){
		this.enjeux.map(
			enjeu => {
				if(('image_url' in enjeu) && ('picto_png' in enjeu)){  // enjeu.image_url !== undefined
					jQuery.get(
						enjeu.image_url,
						data => {
							let classeImg = this.styleEnjeu(enjeu);
							let containerSvg:HTMLElement = document.createElement('div');
							let svgNode = jQuery('svg',data);
							// svgNode.attr('widht','32px');
							// svgNode.attr('height','32px');
							svgNode.addClass('img-fluid img-enjeu-'+classeImg);
							svgNode.addClass('disable-select');

							containerSvg.append(svgNode[0]);
							
							if(!(enjeu.picto_png in this.svgs)){
								this.svgs[enjeu.picto_png] = this.sanitizer.bypassSecurityTrustHtml(containerSvg.innerHTML);
								// console.log('one by one = ',this.svgs);
								// console.log('enjeu.picto_png = ',enjeu.picto_png)
							}
							
						}
					)
				}
			}
		)
	}
	zoomPdg(){
		this.open(this.fichepdg);
	}
}
