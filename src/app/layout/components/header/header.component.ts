import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Client } from '../../../shared/models/client.model';
import * as jQuery from 'jquery';

import * as firebase from 'firebase/app';
import * as CryptoJS from 'crypto-js';



var firebaseConfig = {
    // apiKey: "AIzaSyAEWckegKre1mypuIQHUk-8wXYt64Nu9IM",
    // authDomain: "amplifynature2.firebaseapp.com",
    // databaseURL: "https://amplifynature2.firebaseio.com",
    // projectId: "amplifynature2",
    // storageBucket: "amplifynature2.appspot.com",
    // messagingSenderId: "146345390380",
    // appId: "1:146345390380:web:998cafb93ae2e42d"

    apiKey: "AIzaSyD9iGZ8J8KoOB6eBcXSTmnZjhFGemeHcTk",
    authDomain: "amplifynature-8be2d.firebaseapp.com",
    databaseURL: "https://amplifynature-8be2d.firebaseio.com",
    projectId: "amplifynature-8be2d",
    storageBucket: "amplifynature-8be2d.appspot.com",
    messagingSenderId: "459285736048"


};
var db_app2 = firebase.default.initializeApp(firebaseConfig, 'amplify2-header');

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output() chartClick = new EventEmitter();
    public pushRightClass: string;

    public user: any = {};
    closeResult: string;
    last_conn: Client[];
    customersInitials: any = {};
    hide_profil = false;
    hide_profil1 = false;
    hide_profil2 = false;
    hide_profil3 = false;
    hide_profil4 = false;
    hide_profil5 = false;

    infoTeam:any = {};
    traduction: any = {};
    
    chart = false;
    private userInfo:string;
    private secretKey = "amplifynature-2-2019"


    constructor(private translate: TranslateService, public router: Router,
        private db: AngularFireDatabase,
        private auth: AngularFireAuth,
        private modalService: NgbModal
        ) {

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        let user : Client = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
        if (user) {
            this.translate.use(user && user.langue ? user.langue.toLowerCase() : 'fr');
        }
        else {
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        }

        // console.log("USER %o", user);



        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    generateChartPage(){
        this.chart = !this.chart;
        this.chartClick.emit(this.chart);
    }

    ngOnInit(){
        let self = this;
        this.user = JSON.parse(localStorage.getItem('user'));
        const twentyFirstOrga = [
            'gpa', 'ogic', 'cibex', 'valoptim', 'gpa', 'solvay', 'nexity', 
            'michelin', 'spirit', 'ocr', 'pernod-ricard', 'sanofi', 'yves-rocher', 
            'cemex', 'tereos', 'schneider', 'atland','sareas', 'bpi', 'bouygues'
        ];
        if(this.user){
            // this.auth.auth.signInAndRetrieveDataWithEmailAndPassword(this.user.mail, this.user.password).then((user) =>{
            //     console.log("user signed %o", user)
            //     user.user.getIdToken().then((value) =>{
            //         this.token = value;
            //     })
            // })
            console.log('currentUser = ',this.auth.auth.currentUser);
            let info:any = {};
            info.email = this.user.mail;
            info.password = this.user.password;
            this.userInfo =  CryptoJS.AES.encrypt(JSON.stringify(info), this.secretKey).toString();
            console.log("U :", this.user);
            this.customersInitials['user'] = this.getInitial( this.user.prenom.toString().trim() + ' ' + this.user.nom.toString().trim() );

        }
            let link = (document.querySelector("link[rel*='icon']") || document.createElement('link')) as HTMLLinkElement;
            // link.type = 'image/x-icon';
            // link.rel = 'shortcut icon';
            // link.href = this.user.favicon_img_url+'?='+Math.random();
            // window.document.getElementsByTagName('head')[0].appendChild(link);

            // let link = document.createElement('link') as HTMLLinkElement;
            // let  oldLink = document.getElementById('dynamic-favicon');
            link.id = 'dynamic-favicon';
            link.rel = 'icon';
            link.type = 'image/x-icon';
            // link.href = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9oFFAADATTAuQQAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAAEklEQVQ4y2NgGAWjYBSMAggAAAQQAAGFP6pyAAAAAElFTkSuQmCC";
            link.href = this.user.favicon_img_url;
            window.document.getElementsByTagName('head')[0].appendChild(link);
            // if (oldLink) {
            //   document.head.removeChild(oldLink);
            // }
            // document.head.appendChild(link);

            // this.convertImgToBase64URL(this.user.favicon_img_url, function(base64Img){
            //     link.href = base64Img;
            //     window.document.getElementsByTagName('head')[0].appendChild(link);
            // });
            // console.log("url %o", link.href);
            // console.log("link 3 %o", window.document.getElementsByTagName('head')[0])

         if(!this.user){
             this.router.navigate(['/login']);
         }


        // db_app2.database().ref("clients").once("value",function(data){
        this.db.database.ref("clients").once("value",function(data){

            // for(let i in data.val()){

            //     if(self.user.num != data.val()[i].num && self.user.organisation == data.val()[i].organisation){
            //         self.last_conn.push(data.val()[i]);
            //     }
            // }
            self.db.database.ref("clientsLast").once("value", (dataLast) =>{
                let last:any[] = [];
                dataLast.forEach((dataLastChild) =>{
                    let res:any ={};
                    res.date = dataLastChild.val();
                    res.key = dataLastChild.key;
                    last.push(res);
                })
                self.last_conn = [];
                let equipeFrank: Client;
                data.forEach((child) =>{
                    let client:Client = new Client().deserialize(child.val());
                    client.key = child.key;
                    // console.log(client.key, client.key);
                    if (client.key){
                        // console.log("C :", client.prenom+' '+client.nom);
                        self.customersInitials[client.key] = self.getInitial(client.prenom+' '+client.nom);
                    }

                    let last_connection = last.find((el) => el.key == client.key);
                    if(last_connection){
                        client.last_connection = last_connection.date;
                    }
                    equipeFrank = (client.key == 'client292') && client ;
                    if(self.user.organisation == client.organisation && self.user.mail.toLowerCase() != client.mail.toLocaleLowerCase() && self.last_conn.length < 11){
                        self.last_conn.push(client);
                    }
                })
                // toujours faire apparaitre frankgmail dans liste equipe pour les 20 premier orga dans csv_client client292
                if(!self.last_conn.find( e => e.key.trim() === 'client292' )){
                    if(twentyFirstOrga.includes(self.user.organisation) && self.user.num !== 'client292' && self.user.num === 'client291'){
                        self.last_conn.unshift(equipeFrank);
                    }
                }
                // let dateA = 0;
                // let dateB = 0;

                self.last_conn = self.last_conn.map((a) =>{
                    if( a.last_connection && a.last_connection.split("/").length == 3){
                        a.last_connection = (self.dateFormat(a.last_connection.split("/")[0]))+"/"+(parseInt(self.dateFormat(a.last_connection.split("/")[1]))+1)+"/"+(a.last_connection.split("/")[2]);

                    }
                    return a;
                });
                self.last_conn = self.last_conn.sort(function compare(a, b) {


                    if(a.last_connection && b.last_connection) {

                        let dateA = +new Date(((a.last_connection.split("/")[0])+(a.last_connection.split("/")[1])+(a.last_connection.split("/")[2])));
                        let dateB = +new Date(((b.last_connection.split("/")[0])+(b.last_connection.split("/")[1])+(b.last_connection.split("/")[2])));
                        // dateA = +new Date(a.last_connection) - +new Date(b.last_connection);
                        return dateA - dateB;
                    } else if(a.last_connection && !b.last_connection){
                        return -1;
                    } else if(!a.last_connection && b.last_connection){
                        return 1;
                    } else {
                        return 0;
                    }
                    // let res = dateA - dateB;
                    // console.log("le res: "+res);

                });

                

                console.log("self.last_conn %o",self.last_conn);
            }).catch( exc => console.log('exception pour clientsLast = ',exc) )


        }).catch( exc => console.log('exception pour clients = ',exc) );

        this.db.database.ref("trad").on('value', function(data) {
            data.forEach(function(child) {
              self.traduction[child.val().ref] = child.val();
            });
            console.log("aaaaaaaaaa" , self.traduction);
        });



    }


    dateFormat(val:string) {
        let p = parseInt(val);
        return p < 10 ? "0"+p.toString() : p.toString();

    }

    getInitial(fullName) {
        let result = '';

        let splittedName = fullName.split(' ');

        let string1 = '' , string2 = '';

        if(splittedName.length > 1) {
            string1 = splittedName[0].trim(), string2 = splittedName[1].trim();
        } else if (splittedName.length > 0) {
            string1 = splittedName[0].trim();
                }

        if (string1 && string1.length > 0) {
            result += string1[0];
        }
        if (string2 && string2.length > 0) {
            result += string2[0];
        }
        return result;

    }

    ngAfterViewInit() {
        let self = this;
        this.pushRightClass = 'push-right';



        // if(!this.auth.auth.currentUser){
        //     self.router.navigate(['/login']);
        // }else{
        //     self.db.database.ref("clients").orderByChild("mail").equalTo(self.auth.auth.currentUser.email).once("value", function(snapshot){
        //             if(snapshot.val()){
        //                 for(let i in snapshot.val()){
        //                     self.user = snapshot.val()[i];
        //                     break;
        //                 }
        //             }else{
        //                 self.router.navigate(['/login']);
        //             }


        //         })
        // }
        // this.auth.auth.onAuthStateChanged(function(user){
        //     if(user){
        //         self.db.database.ref("clients").orderByChild("mail").equalTo(self.auth.auth.currentUser.email).once("value", function(snapshot){
        //             if(snapshot.val()){
        //                 for(let i in snapshot.val()){
        //                     self.user = snapshot.val()[i];
        //                     break;
        //                 }
        //             }else{
        //                 self.router.navigate(['/login']);
        //             }


        //         })
        //     }else{
        //         self.router.navigate(['/login']);
        //     }
        // })


    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout(content) {
        this.open(content);
    }

    changeLang(language: string) {
        let user:Client = JSON.parse(localStorage.getItem("user"));
        user.langue = language.toLowerCase();
        localStorage.setItem("user", JSON.stringify(user));
        // window.location.reload();
        this.translate.use(language);
    }

    changeLangToogle() {


        let language:string = (this.translate.currentLang == "fr") ? "en" : "fr";
        let user:Client = JSON.parse(localStorage.getItem("user"));
        user.langue = language.toLowerCase();
        localStorage.setItem("user", JSON.stringify(user));
        this.translate.use(language);
    }

    open(content) {
        this.modalService.open(content, {centered:true}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    deconnexion() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('user');
    }

    checkProfil(client:Client) {


       this.infoTeam[client.key] = true;
       for(let j in this.infoTeam) {
           if(j != client.key) {
               this.infoTeam[j] = false;
           }
       }

    }

    leaveProfil(client:Client) {


        this.infoTeam[client.key] = false;

     }

    checkProfil1() {
        this.hide_profil1 = true;

    }

    checkProfil2() {
        this.hide_profil2 = true;

    }

    checkProfil3() {
        this.hide_profil3 = true;

    }

    checkProfil4() {
        this.hide_profil4 = true;

    }

    checkProfil5() {
        this.hide_profil5 = true;

    }

    leaveProfil1() {
        this.hide_profil1 = false;

    }

    leaveProfil2() {
        this.hide_profil2 = false;
    }

    leaveProfil3() {
        this.hide_profil3 = false;
    }

    leaveProfil4() {
        this.hide_profil4 = false;
    }

    leaveProfil5() {
        this.hide_profil5 = false;
    }

    lang(mot) {
        return this.traduction[mot] !== undefined ? this.traduction[mot][this.translate.currentLang] : mot;
    }

    switchApp() {
        window.open("https://amplifynature2.com", "_blank");
    }
    shadeOfGraysTeam(index:number){
        const graysColor = ['#808080','#DCDCDC','#BBACAC','#C0C0C0','#A9A9A9','#696969','#778899'];
        const color = graysColor[index] ? graysColor[index] : graysColor[0] ;
        return  { 'background-color':color } ;
    }
}
