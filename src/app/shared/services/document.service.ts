import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { CsvPhrase } from "../models/csv-phrase.model";
import { formatDate } from "@angular/common";

const URL  = "https://api.amplifynature.com/index.php/";

@Injectable({
  providedIn: "root"
})


export class DocumentService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public listDocuments(): Observable<CsvPhrase[]> {
    return this.httpClient.get<CsvPhrase[]>(URL + "api/listDocs");
  }

  public listDocumentsByOrga(orga): Observable<CsvPhrase[]> {
    const time = new Date;
    const timeStamp = formatDate(time, "mm:ss", "en-US");
    return this.httpClient.get<CsvPhrase[]>(URL + "api/listDocsWithPhrasesByOrga/" + orga + "/?v=" + timeStamp);
  }

  public listDocumentsWithPhrases(): Observable<CsvPhrase[]> {
    return this.httpClient.get<CsvPhrase[]>(URL + "api/listDocsWithPhrases");
  }

  public listPhrasesByDoc(doc_id): Observable<CsvPhrase[]> {
    return this.httpClient.get<CsvPhrase[]>(URL + "api/listPhrasesByDoc/" + doc_id);
  }

  public listPhrasesByDocWithSeuil(doc_id): Observable<CsvPhrase[]> {
    return this.httpClient.get<CsvPhrase[]>(URL + "api/listPhrasesByDocWithSeuil/" + doc_id);
  }

  public countMondeMaxDocs() {
    return this.httpClient.get<{data: {max: number, extraits: any}}>(URL + "info/countDocumentsMonde/");
  }

  public getEnjeuxWithExtraits() {
    return this.httpClient.get<any[]>(URL + "api/listEnjeuxWithExtraits");
  }

  public getEnjeuExtraits(enjeu: string, fluxArr: string[]) {
    // for (const key in fluxArr) {
    //   fluxArr[key] = encodeURI(fluxArr[key]).replace(/'/g, "%27").replace(/,/g, "%2C").replace(/\(/g, "%28").replace(/\)/g, "%29");
    // }
    // enjeu = escape(enjeu);
    const url = URL + "api/getExtraits/";
    // console.log(url);
    let data = {};
    data["fluxArr"] = fluxArr;
    data["enjeu"] = enjeu;
    const headers = new HttpHeaders({"Content-Type": "application/json"});
    const options = { headers: headers };
    
    // return this.httpClient.post<any>(this.getPhrasesByEnjeu + '/2',enj,options)
    return this.httpClient.post<any>(url, JSON.stringify(data), options);
  }
}
