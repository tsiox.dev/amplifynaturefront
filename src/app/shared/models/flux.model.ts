
import { Deserializable } from "./deserializable.model";

export class Flux implements Deserializable{

	id:string;
	flux:string;
	isMonde:boolean = false;
	group:boolean = false;
	selected:boolean = false;
	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
