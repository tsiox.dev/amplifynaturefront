
import { Deserializable } from "./deserializable.model";
import { Phrase } from "./phrase.model";

export class CsvPhrase implements Deserializable{

	id:number;
	nom:string;
	nom_pdf:string;
	nom_pdg:string;
	pdf_url:string;
	pdg_url:string;
	titre_fr:string;
	titre_en:string;
	auteur:string;
	date:string;
	pays:string;
	lat:number;
	long:number;
	extraits:number;
	phrases:Phrase[];
	valid:boolean = false;
	active:boolean = false;
	csv:string;
	flux:string;
	filename:string;
	url:string;
	sat:any[];
	enjeu_2:any;
	allSat:any[];
	enjeux:any[];
	color:string;
	enjeu_2_color:any[];
	categorie: any;
	
	deserialize(input: any) {
		Object.assign(this, input);
		this.enjeu_2_color = JSON.parse(input.enjeu_2_color);
		this.enjeu_2 = JSON.parse(input.enjeu_2);
	    return this;
	}
}

