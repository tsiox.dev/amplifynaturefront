
import { Deserializable } from "./deserializable.model";

export class Traduction implements Deserializable{

	ref:string;
	fr:string;
	en:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
