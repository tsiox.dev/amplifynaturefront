
import { Deserializable } from "./deserializable.model";

export class Couleur implements Deserializable{

	objet:string;
	couleur:string;
	couleur_annee_existant:string;
	couleur_annee_vide:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

