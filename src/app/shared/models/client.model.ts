
import { Deserializable } from "./deserializable.model";
import { Flux } from "./flux.model";

export class Client implements Deserializable{

	key:string;
	num:string;
	prenom:string;
	nom:string;
	mail:string;
	organisation:string;
	orga:string[];
	poste:string;
	langue:string;
	password:string;
	last_connection:string;
	orga_img_url:string;
	frontoffice_img_url:string;
	favicon_img_url:string;
	favicon2_img_url:string;
	orga_png:string;
	img_url:string;
	client_png:string;
	favicon_png:string;
	favicon2_png:string;
	frontoffice_png:string;
	mots_cles:string;
	flux:Flux[] = [];
	fluxSelected:Flux[] = [];
	app_1:string;
	app_2:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

export class Orga implements Deserializable{

	
	organisation:string;
	key:string;
	
	orga_img_url:string;
	frontoffice_img_url:string;
	favicon_img_url:string;
	favicon2_img_url:string;
	orga_png:string;
	favicon_png:string;
	favicon2_png:string;
	frontoffice_png:string;
	flux:Flux[];
	site:string;
	site_2:string;
	site_3:string;
	site_4:string;
	site_5:string;
	concurrents:string[];
	type:string;
	app_1:string;
	app_2:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
