
import { Deserializable } from "./deserializable.model";
import { Enjeu } from "./enjeu.model";

export class Phrase implements Deserializable{

	document_id:number;
	num:string;
	langue:string;
	compteur_perf:string;
	performance:string;
	enjeu:any = 0;
	enjeu_2_color:string;
	satisfaction:number = 1;
	phrase_en:string;
	phrase_fr:string;
	phrase:string;
	flux:string;
	csv_phrase_id:string;
	valid:boolean = false;

	deserialize(input: any) {
	    Object.assign(this, input);
	    if(this.satisfaction == 0){
	    	this.satisfaction = 1;
		}
		if(input.enjeu_2_color){
			// console.log(input.enjeu_2_color);
			this.enjeu_2_color = JSON.parse(JSON.stringify(input.enjeu_2_color.trim()));
		}
	    return this;
	}
}

